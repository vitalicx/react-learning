import React from "react";
import { shallow } from "enzyme";
import SearchInput from "../../components/inputs/SearchInput";
import SearchInputContainer from "./SearchInputContainer";

describe("<SearchInputContainer />", () => {
  const props = {
    field: {
      name: "test",
    },
    form: {
      errors: {
        test: "test error",
      },
      touched: {
        test: true,
      },
      values: {
        test: "test",
      },
      setFieldTouched: jest.fn(),
      validateForm: jest.fn(),
      resetForm: jest.fn(),
      isValid: true,
    },
    required: true,
    onSearch: jest.fn(),
    onClear: jest.fn(),
    isLoading: false,
  };

  beforeEach(() => {
    props.onSearch.mockClear();
  });

  describe("by default", () => {
    const wrap = shallow(<SearchInputContainer {...props} />);

    it("renders a single `SearchInput`", () => {
      expect(wrap.type()).toBe(SearchInput);
    });

    it("passes required props to `SearchInput` component", () => {
      expect(wrap.prop("form")).toBe(props.form);
      expect(wrap.prop("field")).toBe(props.field);
      expect(wrap.prop("isLoading")).toBe(false);
    });

    it("passes any additional props to `SearchInput` component", () => {
      expect(wrap.prop("required")).toBeTruthy();
    });

    it("should handle searching", async () => {
      await wrap.props().onSearch("test search");
      expect(props.onSearch).toHaveBeenCalledWith("test search");
      expect(props.form.setFieldTouched).toHaveBeenCalledWith("test", true);
      expect(props.form.validateForm).toHaveBeenCalled();
    });

    it("should handle clearing the form", () => {
      const mockRef = { focus: jest.fn() };
      wrap.props().setInput(mockRef);
      wrap.props().onClear();
      expect(mockRef.focus).toHaveBeenCalled();
      expect(props.form.resetForm).toHaveBeenCalled();
      expect(props.onClear).toHaveBeenCalled();
    });
  });

  describe("when the form is invalid", () => {
    const invalidProps = { ...props, form: { ...props.form, isValid: false } };
    const wrap = shallow(<SearchInputContainer {...invalidProps} />);

    it("should not perform a search", () => {
      wrap.props().onSearch("test search");
      expect(props.onSearch).not.toHaveBeenCalled();
    });
  });
});
