import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import SearchInput from "../../components/inputs/SearchInput";

class SearchInputContainer extends PureComponent {
  static propTypes = {
    onSearch: PropTypes.func.isRequired,
    onClear: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    form: PropTypes.shape({
      setFieldTouched: PropTypes.func.isRequired,
      validateForm: PropTypes.func.isRequired,
      resetForm: PropTypes.func.isRequired,
      isValid: PropTypes.bool.isRequired
    }),
    field: PropTypes.shape({
      name: PropTypes.string.isRequired
    })
  };

  handleSearch = async text => {
    const { form, field: { name }, onSearch } = this.props;
    form.setFieldTouched(name, true);
    await form.validateForm();
    if (form.isValid) {
      onSearch(text);
    }
  };

  handleClear = () => {
    this.textInput.focus();
    this.props.form.resetForm();
    this.props.onClear();
  };

  setInput = input => {
    this.textInput = input;
  };

  render() {
    const {
      form,
      field,
      isLoading,
      // eslint-disable-next-line no-unused-vars
      onClear, onSearch,
      ...props
    } = this.props;

    return (
      <SearchInput
        form={form}
        field={field}
        isLoading={isLoading}
        onClear={this.handleClear}
        onSearch={this.handleSearch}
        setInput={this.setInput}
        {...props}
      />
    );
  }
}

export default SearchInputContainer;
