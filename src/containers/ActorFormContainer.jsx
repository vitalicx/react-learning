import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { requestMovies } from "../actions/fields/movies";
import ActorForm from "../components/ActorForm";
import { getMovies, getIsLoading, getError } from "./../reducers/ui/fields/movies";
import { getToken } from "./../reducers/user";
import debounce from "lodash/debounce";

export class ActorFormContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.handleSearch = debounce(this.handleSearch.bind(this), 200);
    this.handleReset = this.handleReset.bind(this);
  }

  static propTypes = {
    movie: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    // mapStateToProps
    movieSelectData: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
  };

  state = {
    movieSelectTouched: false,
  };

  getMovieSelectData = () => {
    const { movie, movieSelectData } = this.props;
    return false === this.state.movieSelectTouched ? [movie] : movieSelectData;
  };

  handleSearch(text) {
    this.setState({ movieSelectTouched: !!text });
    if (text.length > 0) {
      this.props.requestMovies(this.props.token, text);
    }
  }

  handleReset() {
    this.setState({ movieSelectTouched: false });
  }

  render() {
    const { isLoading, movie, onSubmit } = this.props;
    return (
      <ActorForm
        isLoading={isLoading}
        movieSelectDefault={[`${movie.id}`]}
        movieSelectData={this.getMovieSelectData()}
        onSearch={this.handleSearch}
        onReset={this.handleReset}
        onSubmit={onSubmit}
        resetAfterSubmit
      />
    );
  }
}

export const mapStateToProps = state => ({
  movieSelectData: getMovies(state),
  isLoading: getIsLoading(state),
  error: getError(state),
  token: getToken(state),
});

export default connect(
  mapStateToProps,
  { requestMovies }
)(ActorFormContainer);
