import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteMovie, deleteMovieMultiple, increaseCount, increaseCountMultiple } from "../actions/movies";
import MovieList from "../components/MovieList";
import { moviesPaginator } from "../paginators";
import { getActions } from "../reducers/movies/actions";
import { getToken } from "../reducers/user";
import { getCurrentPage } from "./../helpers/pagination/selectors";

const { getCurrentPageResults, requestPage, resetPages } = moviesPaginator;

export class MovieListContainer extends Component {
  constructor(props) {
    super(props);
    if (!props.movies.length) {
      this.props.requestPage(1);
    }
  }

  static propTypes = {
    actions: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
    movies: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    currentPage: PropTypes.number.isRequired,
  };

  handleIncrease = data => {
    if (Array.isArray(data)) {
      this.props.increaseCountMultiple(data, this.props.token);
    } else {
      this.props.increaseCount(data, this.props.token);
    }
  };

  handleDelete = data => {
    if (Array.isArray(data)) {
      this.props.deleteMovieMultiple(data, this.props.token);
    } else {
      this.props.deleteMovie(data.id, this.props.token);
    }
  };

  render() {
    const { movies, actions, isLoading } = this.props;
    return (
      <MovieList
        stateKey="movies"
        movies={movies}
        actions={actions}
        onIncrease={this.handleIncrease}
        onDelete={this.handleDelete}
        isLoading={isLoading}
      />
    );
  }
}

export const mapStateToProps = state => ({
  actions: getActions(state),
  token: getToken(state),
  movies: getCurrentPageResults(state),
  currentPage: getCurrentPage(state, "movies"),
});

export default connect(
  mapStateToProps,
  {
    requestPage,
    resetPages,
    deleteMovie,
    deleteMovieMultiple,
    increaseCount,
    increaseCountMultiple,
  }
)(MovieListContainer);
