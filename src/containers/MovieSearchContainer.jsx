import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import MovieSearchForm from "../components/MovieSearchForm";
import { getPageFilter, getPageIsLoading } from "../helpers/pagination/selectors";

export class MovieSearchContainer extends Component {
  static propTypes = {
    filter: PropTypes.object.isRequired,
    onSearch: PropTypes.func.isRequired,
  };

  handleClear = () => !!this.props.filter.title && this.props.onSearch();

  render() {
    return (
      <MovieSearchForm
        onSearch={this.props.onSearch}
        onClear={this.handleClear}
        isLoading={this.props.isLoading}
      />
    );
  }
}

export const mapStateToProps = state => ({
  filter: getPageFilter(state, "movies"),
  isLoading: getPageIsLoading(state, "movies"),
});

export default connect(mapStateToProps)(MovieSearchContainer);
