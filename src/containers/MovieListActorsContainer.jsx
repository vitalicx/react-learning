import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteActor } from "../actions/movies";
import { addActor } from "../actions/actors";
import MovieListActors from "../components/MovieListActors";
import { getToken } from "../reducers/user";
import { getActions, getIsDeletingActor } from "../reducers/movies/actions";
import { getActorsForMovie } from "../reducers/actors";
import ActorFormContainer from "./ActorFormContainer";

export class MovieListActorsContainer extends Component {
  constructor(props) {
    super(props);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.isDeleting = this.isDeleting.bind(this);
  }

  static propTypes = {
    movie: PropTypes.object.isRequired,
    // mapStateToProps
    actors: PropTypes.array.isRequired,
    token: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired,
  };

  async handleAdd(actor) {
    await this.props.addActor(actor, this.props.token);
  }

  async handleDelete(actor) {
    await this.props.deleteActor(this.props.movie, actor, this.props.token);
  }

  isDeleting(actor) {
    return getIsDeletingActor(this.props.actions, this.props.movie.id, actor.id);
  }

  render() {
    return (
      <>
        <MovieListActors
          actors={this.props.actors}
          isDeleting={this.isDeleting}
          onDelete={this.handleDelete}
        />
        <ActorFormContainer onSubmit={this.handleAdd} movie={this.props.movie} />
      </>
    );
  }
}

export const mapStateToProps = (state, props) => ({
  actors: getActorsForMovie(state, props.movie),
  actions: getActions(state),
  token: getToken(state),
});

export default connect(
  mapStateToProps,
  { deleteActor, addActor }
)(MovieListActorsContainer);
