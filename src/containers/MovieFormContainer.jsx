import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { requestActors } from "../actions/fields/actors";
import MovieForm from "../components/MovieForm";
import { getActors, getIsLoading, getError } from "./../reducers/ui/fields/actors";
import { getToken } from "./../reducers/user";
import debounce from "lodash/debounce";

export class MovieFormContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.handleSearch = debounce(this.handleSearch.bind(this), 200);
  }

  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    // mapStateToProps
    actors: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
  };

  handleSearch(text) {
    this.props.requestActors(this.props.token, text);
  }

  render() {
    const { isLoading, actors, onSubmit } = this.props;
    return (
      <MovieForm
        isLoading={isLoading}
        actors={actors}
        onSearch={this.handleSearch}
        onSubmit={onSubmit}
        resetAfterSubmit
      />
    );
  }
}

export const mapStateToProps = state => ({
  actors: getActors(state),
  isLoading: getIsLoading(state),
  error: getError(state),
  token: getToken(state),
});

export default connect(
  mapStateToProps,
  { requestActors }
)(MovieFormContainer);
