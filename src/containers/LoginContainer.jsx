import React, { Component } from "react";
import { connect } from "react-redux";
import { login } from "../actions/user";
import LoginForm from "../components/LoginForm";

export class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
  }

  async handleLogin(data) {
    await this.props.login(data);
  }

  render() {
    return (
      <LoginForm onSubmit={this.handleLogin} />
    );
  }
}

export default connect(null, { login })(LoginContainer);