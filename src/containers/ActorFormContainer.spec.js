import React from "react";
import { shallow } from "enzyme";
import { ActorFormContainer, mapStateToProps } from "./ActorFormContainer";
import reducer from "../reducers/index";
import ActorForm from "../components/ActorForm";

jest.mock("../components/ActorForm", () => () => <></>);
jest.useFakeTimers();

describe("<ActorFormContainer />", () => {
  const token = "test";
  const movies = [{ id: 0 }, { id: 1 }];
  const props = {
    requestMovies: jest.fn(),
    onSubmit: jest.fn(),
    movieSelectData: movies,
    movie: movies[0],
    isLoading: false,
    error: {},
    token,
  };

  const wrap = shallow(<ActorFormContainer {...props} />);

  beforeEach(() => {
    wrap.props().onReset();
    props.requestMovies.mockClear();
  });

  it("should render a single `ActorForm` component", () => {
    expect(wrap.type()).toBe(ActorForm);
  });

  it("should handle a search request from the movies field", () => {
    wrap.props().onSearch("test");
    jest.runAllTimers();
    expect(props.requestMovies).toBeCalledWith(token, "test");
  });

  it("should handle resetting the form", () => {
    wrap.props().onSearch("test");
    jest.runAllTimers();
    wrap.props().onReset();
    expect(wrap.prop("movieSelectData")).toEqual([props.movie]);
  });

  describe("rendered `ActorForm`", () => {
    it("should pass `onSubmit` prop", () => {
      wrap.props().onSubmit();
      expect(props.onSubmit).toHaveBeenCalled();
    });

    it("should set `movieSelectDefault` prop to an array containing passed movie ID", () => {
      expect(wrap.prop("movieSelectDefault").includes(`${props.movie.id}`)).toBeTruthy();
    });

    describe("when the user has not searched for a movie", () => {
      it("should set `movieSelectData` prop to an array containing the passed movie", () => {
        expect(wrap.prop("movieSelectData")).toEqual([props.movie]);
      });
    });

    describe("when the user has searched for a movie", () => {
      it("should set `movieSelectData` prop to passed `movieSelectData` prop", () => {
        wrap.props().onSearch("test");
        jest.runAllTimers();
        expect(wrap.prop("movieSelectData")).toBe(props.movieSelectData);
      });
    });

    describe("when `onSearch` callback is invoked with an empty string", () => {
      it("should not request movies", () => {
        wrap.props().onSearch("");
        jest.runAllTimers();
        expect(props.requestMovies).not.toHaveBeenCalled();
      });
    });
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }));
    expect(props).toMatchSnapshot();
  });
});
