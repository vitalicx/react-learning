import React from "react";
import { shallow } from "enzyme";
import { MovieFormContainer, mapStateToProps } from "./MovieFormContainer";
import reducer from "../reducers/index";
import MovieForm from "../components/MovieForm";

jest.mock("../components/MovieForm", () => () => <></>);
jest.useFakeTimers();

describe("<MovieFormContainer />", () => {
  const token = "test";
  const actors = [{ id: 0 }];

  const actions = {
    requestActors: jest.fn(),
    onSubmit: jest.fn(),
  };

  const wrap = shallow(
    <MovieFormContainer actors={actors} isLoading={false} error={{}} token={token} {...actions} />
  );

  it("should render a single `MovieForm` component", () => {
    expect(wrap.type()).toBe(MovieForm);
  });

  it("should pass `onSubmit` prop through to `MovieForm`", () => {
    wrap.props().onSubmit();
    expect(actions.onSubmit).toHaveBeenCalled();
  });

  it("should pass `actors` prop through to `MovieForm`", () => {
    expect(wrap.prop("actors")).toBe(actors);
  });

  it("should handle a search request from the actors field", () => {
    wrap.props().onSearch("test");
    jest.runAllTimers();
    expect(actions.requestActors).toBeCalledWith(token, "test");
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }));
    expect(props).toMatchSnapshot();
  });
});
