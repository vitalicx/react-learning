import React from "react";
import { shallow } from "enzyme";
import { LoginContainer } from "./LoginContainer";

jest.mock("../components/LoginForm", () => () => <></>);

describe("<LoginContainer />", () => {
  it("should render correctly", () => {
    const mockLogin = jest.fn();
    const wrap = shallow(<LoginContainer login={mockLogin} />);
    wrap.prop("onSubmit")({});
    expect(mockLogin).toHaveBeenCalledWith(expect.any(Object));
  });
});
