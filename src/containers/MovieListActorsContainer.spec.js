import React from "react";
import { shallow } from "enzyme";
import { MovieListActorsContainer, mapStateToProps } from "./MovieListActorsContainer";
import reducer from "../reducers/index";
import MovieListActors from "./../components/MovieListActors";
import ActorFormContainer from "../containers/ActorFormContainer";
import ActorForm from "../components/ActorForm";

jest.mock("../components/MovieListActors", () => () => <></>);
jest.mock("../containers/ActorFormContainer", () => () => <></>);

describe("<MovieFormContainer />", () => {
  const token = "test";
  const movie = { id: 0, actors: [{ id: 0 }] };

  const addActorSpy = jest.fn();
  const deleteActorSpy = jest.fn();

  const wrap = shallow(
    <MovieListActorsContainer
      movie={movie}
      actors={movie.actors}
      token={token}
      actions={{ deleteActor: { 0: [0] } }}
      addActor={addActorSpy}
      deleteActor={deleteActorSpy}
    />
  );

  it("should render a `MovieListActors` component", () => {
    expect(wrap.find(MovieListActors).exists()).toBeTruthy();
  });

  it("should pass `actors` prop to `MovieListActors` component", () => {
    expect(wrap.find(MovieListActors).prop("actors")).toBe(movie.actors);
  });

  it("should provide a function to determine whether an actor is being deleted", () => {
    expect(wrap.find(MovieListActors).props().isDeleting(movie, movie.actors[0])).toBeTruthy();
  });

  it("should handle an actor being deleted", () => {
    wrap.find(MovieListActors).props().onDelete(movie.actors[0]);
    expect(deleteActorSpy).toHaveBeenCalledWith(movie, movie.actors[0], token);
  });

  it("should render an `ActorFormContainer` component", () => {
    expect(wrap.find(ActorFormContainer).exists()).toBeTruthy();
    expect(wrap.find(ActorFormContainer).prop("movie")).toBe(movie);
  });

  it("should handle an actor being added", () => {
    wrap.find(ActorFormContainer).props().onSubmit();
    expect(addActorSpy).toHaveBeenCalled();
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }), { movie });
    expect(props).toMatchSnapshot();
  });
});
