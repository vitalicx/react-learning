import React from "react";
import { shallow } from "enzyme";
import { MoviesContainer, mapStateToProps } from "./MoviesContainer";
import reducer from "../reducers/index";

jest.mock("../components/Movies", () => ({
  Movies: () => <></>,
}));

describe("<MoviesContainer />", () => {
  const token = "test";
  const movie = { id: 0 };

  const actions = {
    addMovie: jest.fn(),
    requestPage: jest.fn(),
    resetPages: jest.fn(),
    updateFilter: jest.fn(),
    trimFilter: jest.fn(),
  };

  const wrap = shallow(
    <MoviesContainer token={token} isLoading={false} currentPage={1} {...actions} />
  );

  it("should handle a call to add a movie", async () => {
    await wrap.prop("actions").handleAdd(movie);
    expect(actions.addMovie).toHaveBeenCalledWith(movie, token);
    expect(actions.resetPages).toHaveBeenCalledWith(1);
  });

  it("should handle a call to attempt re-requesting movies", () => {
    wrap.prop("actions").handleRetry();
    expect(actions.requestPage).toHaveBeenCalledWith(1);
  });

  describe("when a movie search is performed", () => {
    afterEach(() => {
      expect(actions.resetPages).toHaveBeenCalled();
      expect(actions.requestPage).toHaveBeenCalledWith(1);
    });
    describe("and a search term is provided", () => {
      beforeEach(() => {
        wrap.prop("actions").handleSearch("test");
      });
      it("should call `updateFilter`", () => {
        expect(actions.updateFilter).toHaveBeenCalledWith({ title: "test" });
      });
    });
    describe("and a search term is not provided", () => {
      beforeEach(() => {
        wrap.prop("actions").handleSearch();
      });
      it("should call `trimFilter`", () => {
        expect(actions.trimFilter).toHaveBeenCalledWith("title");
      });
    });
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }));
    expect(props).toMatchSnapshot();
  });

  it("should receive false as `isLoading` prop when refreshing cached page", () => {
    const state = reducer(undefined, { type: "unknown" });
    const paginations = { movies: { isLoading: true, currentPage: 1, pages: { 1: [] } } };
    const props = mapStateToProps({ ...state, paginations });
    expect(props.isLoading).toBe(false);
  });
});
