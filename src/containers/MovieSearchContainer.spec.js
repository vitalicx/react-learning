import React from "react";
import { shallow } from "enzyme";
import reducer from "../reducers/index";
import { MovieSearchContainer, mapStateToProps } from "./MovieSearchContainer";
import MovieSearchForm from "../components/MovieSearchForm";

describe("<MovieSearchContainer />", () => {
  const onSearchSpy = jest.fn();

  beforeEach(() => {
    onSearchSpy.mockClear();
  });

  describe("by default", () => {
    const wrap = shallow(
      <MovieSearchContainer onSearch={onSearchSpy} isLoading={false} filter={{}} />
    );

    it("renders a single `MovieSearchForm`", () => {
      expect(wrap.is(MovieSearchForm)).toBeTruthy();
    });

    it("passes `isLoading` prop to `MovieSearchForm`", () => {
      expect(wrap.prop("isLoading")).toBe(false);
    });

    it("passes `onSearch` prop to `MovieSearchForm`", () => {
      wrap.props().onSearch();
      expect(onSearchSpy).toHaveBeenCalled();
    });

    describe("the `onClear` callback passed to `MovieSearchForm`", () => {
      it("does not call `onSearch` prop if a search has not been performed", () => {
        wrap.props().onClear();
        expect(onSearchSpy).not.toHaveBeenCalled();
      });
    });
  });

  describe("when a search has been performed", () => {
    const wrap = shallow(
      <MovieSearchContainer onSearch={onSearchSpy} isLoading={false} filter={{ title: "test" }} />
    );

    describe("the `onClear` callback passed to `MovieSearchForm`", () => {
      it("calls `onSearch` prop", () => {
        wrap.props().onClear();
        expect(onSearchSpy).toHaveBeenCalled();
      });
    });
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }));
    expect(props).toMatchSnapshot();
  });
});
