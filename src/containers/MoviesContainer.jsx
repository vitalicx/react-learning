import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addMovie } from "../actions/movies";
import { Movies } from "../components/Movies";
import { moviesPaginator } from "../paginators";
import { getToken } from "../reducers/user";
import {
  getPageError,
  getPageIsLoading,
  getCurrentPage,
  getPageIds,
} from "./../helpers/pagination/selectors";

const { requestPage, updateFilter, trimFilter, resetPages } = moviesPaginator;

export class MoviesContainer extends Component {
  constructor(props) {
    super(props);
    this.actions = {
      handleAdd: this.handleAdd.bind(this),
      handleRetry: this.handleRetry.bind(this),
      handleSearch: this.handleSearch.bind(this),
    };
  }

  static propTypes = {
    currentPage: PropTypes.number.isRequired,
    isLoading: PropTypes.bool.isRequired,
    token: PropTypes.string.isRequired,
    error: PropTypes.object,
  };

  async handleAdd(movie) {
    await this.props.addMovie(movie, this.props.token);
    this.props.resetPages(this.props.currentPage);
  }

  handleRetry() {
    this.props.requestPage(this.props.currentPage);
  }

  handleSearch(text) {
    this.props.resetPages();
    if (text) {
      this.props.updateFilter({ title: text });
    } else {
      this.props.trimFilter("title");
    }
    this.props.requestPage(1);
  }

  render() {
    return (
      <Movies isLoading={this.props.isLoading} error={this.props.error} actions={this.actions} />
    );
  }
}

export const mapStateToProps = state => {
  const currentPage = getCurrentPage(state, "movies");
  const isLoading = getPageIsLoading(state, "movies") && !getPageIds(state, "movies", currentPage);
  return {
    isLoading,
    currentPage,
    error: getPageError(state, "movies"),
    token: getToken(state),
  };
};

export default connect(
  mapStateToProps,
  { addMovie, requestPage, updateFilter, trimFilter, resetPages }
)(MoviesContainer);
