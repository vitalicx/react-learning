import React from "react";
import { shallow } from "enzyme";
import { MovieListContainer, mapStateToProps } from "./MovieListContainer";
import MovieList from "../components/MovieList";
import reducer from "../reducers/index";

describe("<MovieListContainer />", () => {
  let props, mountedComponent;

  const increaseCountSpy = jest.fn();
  const increaseCountMultipleSpy = jest.fn();
  const deleteMovieSpy = jest.fn();
  const deleteMovieMultipleSpy = jest.fn();
  const requestPageSpy = jest.fn();
  const resetPagesSpy = jest.fn();

  beforeEach(() => {
    requestPageSpy.mockClear();
    resetPagesSpy.mockClear();
    props = {
      currentPage: 1,
      movies: [{ title: "test", id: 1 }, { title: "test two", id: 2 }],
      actions: {},
    };
    mountedComponent = undefined;
  });

  const wrap = () => {
    if (!mountedComponent) {
      mountedComponent = shallow(
        <MovieListContainer
          {...props}
          token="test"
          isLoading={true}
          increaseCount={increaseCountSpy}
          increaseCountMultiple={increaseCountMultipleSpy}
          deleteMovie={deleteMovieSpy}
          deleteMovieMultiple={deleteMovieMultipleSpy}
          requestPage={requestPageSpy}
          resetPages={resetPagesSpy}
        />
      );
    }
    return mountedComponent;
  };

  it("renders a `MovieList`", () => {
    expect(wrap().is(MovieList)).toBeTruthy();
  });

  it("passes `movies` prop to `MovieList`", () => {
    expect(wrap().prop("movies")).toEqual(props.movies);
  });

  it("passes `actions` prop to `MovieList`", () => {
    expect(wrap().prop("actions")).toEqual(props.actions);
  });

  it("passes `isLoading` prop to `MovieList`", () => {
    expect(wrap().prop("isLoading")).toBe(true);
  });

  it("handles increasing track count", () => {
    wrap().props().onIncrease(props.movies[0]);
    expect(increaseCountSpy).toHaveBeenCalledWith(props.movies[0], "test");
  });

  it("handles increasing multiple track counts", () => {
    wrap().props().onIncrease(props.movies);
    expect(increaseCountSpy).toHaveBeenCalledWith(props.movies[0], "test");
  });

  it("handles deleting a movie", async () => {
    await wrap().props().onDelete(props.movies[0]);
    expect(deleteMovieSpy).toHaveBeenCalledWith(props.movies[0].id, "test");
  });

  it("handles deleting multiple movies", async () => {
    await wrap().props().onDelete(props.movies);
    expect(deleteMovieMultipleSpy).toHaveBeenCalledWith(props.movies, "test");
  });

  describe("when passed `movies` prop array is empty", () => {
    beforeEach(() => {
      props.movies = [];
    });
    it("should request movies from server", () => {
      // Call wrap() to force a render with empty movies prop
      wrap();
      expect(requestPageSpy).toHaveBeenCalledWith(1);
    });
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }));
    expect(props).toMatchSnapshot();
  });
});
