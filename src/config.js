export const API_BASE_URL = "http://localhost:8080";

export const API_DEFAULT_HEADERS = {
  "Content-Type": "application/json",
  "Accept": "application/json",
};