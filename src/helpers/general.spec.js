import { deepMap } from "./general";

describe("When deep mapping an object", () => {
  const initialState = {
    user: {
      data: {
        expiry: "2017-11-25T12:34:56z",
        refreshToken: "test",
      },
    },
  };

  it("should replace date strings with Date objects", () => {
    const finalState = deepMap(initialState, val => (isNaN(Date.parse(val)) ? val : new Date(val)));
    expect(finalState.user.data.expiry).toBeInstanceOf(Date);
  });
});
