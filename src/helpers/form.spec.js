import React from "react";
import createForm from "./form";
import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import { mockErrorOnce } from "../helpers/testing";
import { withFormik } from "formik";
import { withTemplate } from "../components/TemplatedForm";

const mockSetSubmitting = jest.fn();
const mockSetFieldError = jest.fn();
const mockResetForm = jest.fn();

jest.mock("formik", () => ({
  withFormik: jest.fn(config => Component => props => {
    const handleSubmit = event =>
      config.handleSubmit(event.data, {
        props,
        setSubmitting: mockSetSubmitting,
        setFieldError: mockSetFieldError,
        resetForm: mockResetForm,
      });
    return <Component handleSubmit={handleSubmit} />;
  }),
}));

jest.mock("../components/TemplatedForm", () => ({
  withTemplate: jest.fn(Fields => {
    return function MockFormOuter({ handleSubmit }) {
      return (
        <form onSubmit={handleSubmit}>
          <Fields />
        </form>
      );
    };
  }),
}));

describe("Form creator", () => {
  const fields = () => <div />;
  const testConfig = { test: "test" };
  const Form = createForm(testConfig)(fields);
  const onSubmit = jest.fn();
  const wrap = mount(<Form onSubmit={onSubmit} />);

  it("should create a templated form", () => {
    expect(withFormik).toHaveBeenCalledWith(
      expect.objectContaining({
        handleSubmit: expect.any(Function),
        ...testConfig
      })
    );
    expect(withTemplate).toHaveBeenCalledWith(fields);
    expect(toJson(wrap)).toMatchSnapshot();
  });

  it("should handle a successful submit", async () => {
    const data = { test: "test" };
    await wrap.find("form").simulate("submit", { data });
    expect(onSubmit).toHaveBeenCalledWith(data);
    expect(mockSetSubmitting).toHaveBeenCalledWith(false);
  });

  it("should reset the form after submit", async () => {
    expect(mockResetForm).not.toHaveBeenCalled();
    wrap.setProps({ resetAfterSubmit: true });
    await wrap.find("form").simulate("submit");
    expect(mockResetForm).toHaveBeenCalled();
  });

  it("should handle an unsuccessful submit", () => {
    const violation = { propertyPath: "test", message: "test" };
    mockErrorOnce(onSubmit, [violation]);
    wrap.find("form").simulate("submit");
    expect(mockSetFieldError).toHaveBeenCalledWith(violation.propertyPath, violation.message);
  });
});
