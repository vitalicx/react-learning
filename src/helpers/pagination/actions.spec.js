import * as actions from "./actions";

describe("Pagination actions", () => {
  const dispatch = jest.fn();
  const refreshPageSpy = jest.fn();

  beforeEach(() => {
    dispatch.mockClear();
  });

  it("should create an action to reset the pages cache", () => {
    const thunk = actions.resetPages("test");
    expect(thunk).toEqual(expect.any(Function));
    thunk(dispatch);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        meta: expect.objectContaining({
          key: "test",
        }),
        payload: expect.objectContaining({
          currentPage: undefined,
        }),
      })
    );
    expect(refreshPageSpy).not.toHaveBeenCalled();
  });

  it("should dispatch REFRESH_PAGE when `resetPages` called with `currentPage` argument", () => {
    actions.resetPages("test", refreshPageSpy, 1)(dispatch);
    expect(refreshPageSpy).toHaveBeenCalledWith(1);
  });
});
