import Types from "../../constants/action-types";
import { asyncFetch } from "../fetch";
import { getToken } from "../../reducers/user";
import { receivePage, requestPageFailure } from "./actions";
import { getPageIds, getPageSize, getPageFilter } from "./selectors";

const getTotal = response =>
  parseInt(
    response.headers
      .get("Content-Range")
      .split("/")
      .slice(-1)[0]
  );

export const getSortParams = (sortField, sortOrder) => sortField && ({
  [`order[${sortField}]`]: sortOrder === "ascend" ? "asc" : "desc",
});

const paginatorMiddleware = ({ dispatch, getState }) => next => action => {
  if (Types.REQUEST_PAGE === action.type) {
    const {
      meta: { key, endpoint, normalizer, refresh },
      payload: { page, path },
    } = action;
    const state = getState();

    if (false === refresh && getPageIds(state, key, page)) {
      return next({ ...action, meta: { ...action.meta, fromCache: true } });
    }

    const { sortField, sortOrder, ...filter } = getPageFilter(state, key);
    const sortParams = getSortParams(sortField, sortOrder);
    const pageSize = getPageSize(state, key);

    dispatch(async dispatch => {
      try {
        const url = endpoint + path;
        const token = getToken(state);
        const query = { page, pageSize, ...sortParams, ...filter };
        const response = await asyncFetch(url, {
          method: "GET",
          token,
          query,
          json: false,
        });
        const total = getTotal(response);
        const data = normalizer(await response.json());
        dispatch(receivePage(key, page, { ...data, total }));
      } catch (error) {
        dispatch(requestPageFailure(key, page, error));
      }
    });
  }

  return next(action);
};

export default paginatorMiddleware;
