import { createSelector } from "reselect";

const getPagination = (state, key) => state.paginations[key];

const getPageIsLoading = (state, key) => state.paginations[key].isLoading;
const getPageError = (state, key) => state.paginations[key].error;
const getCurrentPage = (state, key) => state.paginations[key].currentPage;
const getPageFilter = (state, key) => state.paginations[key].filter;
const getPageSize = (state, key) => state.paginations[key].pageSize;

const getPageIds = (state, key, page) => state.paginations[key].pages[page];
const getCurrentPageIds = (state, key) => state.paginations[key].pages[getCurrentPage(state, key)];

const currentPageResultsFactory = key =>
  createSelector(
    [state => state[key].byId, state => getCurrentPageIds(state, key)],
    (movies, ids) => (ids ? ids.filter(id => movies[id]).map(id => movies[id]) : [])
  );

export {
  getPageError,
  getPageIsLoading,
  getCurrentPage,
  getPageFilter,
  getPageSize,
  getPageIds,
  getPagination,
  currentPageResultsFactory,
};
