import createPaginator from "./createPaginator";
import reducers from "./reducers";
import {
  requestPage,
  resetPages,
  setPageSize,
  setPageSorting,
  trimFilter,
  updateFilter,
} from "./actions";

jest.mock("./reducers");
jest.mock("./actions", () => ({
  requestPage: jest.fn(),
  resetPages: jest.fn(),
  setPageSize: jest.fn(),
  setPageSorting: jest.fn(),
  updateFilter: jest.fn(),
  trimFilter: jest.fn(),
}));

describe("createPaginator", () => {
  beforeEach(() => {
    requestPage.mockClear();
  });

  const paginator = createPaginator("test", "http://test");

  it("should create a paginator object", () => {
    expect(paginator).toMatchObject({
      reducers: expect.any(Function),
      requestPage: expect.any(Function),
      refreshPage: expect.any(Function),
      getCurrentPageResults: expect.any(Function),
      resetPages: expect.any(Function),
      setPageSize: expect.any(Function),
      setPageSorting: expect.any(Function),
      updateFilter: expect.any(Function),
      trimFilter: expect.any(Function),
    });
  });

  it("should return a reducer that is targeted to the provided key", () => {
    const reducer = paginator.reducers;
    const testAction = { meta: { key: "test" } };
    reducer(undefined, testAction);
    expect(reducers).toHaveBeenCalled();
    reducers.mockClear();
    const testOtherAction = { meta: { key: "testOther" } };
    reducer(undefined, testOtherAction);
    expect(reducers).not.toHaveBeenCalled();
  });

  it("should return a reducer that handles undefined arguments", () => {
    const reducer = paginator.reducers;
    expect(reducer()).toBeUndefined();
  });

  it("should return a function to request a page", () => {
    paginator.requestPage();
    expect(requestPage).toHaveBeenCalled();
  });

  it("should return a function to refresh a page", () => {
    paginator.refreshPage(1);
    expect(requestPage).toHaveBeenCalled();
    // `refresh` argument should be true
    expect(requestPage.mock.calls[0][3]).toBe(true);
    // `page` argument should be the one passed
    expect(requestPage.mock.calls[0][4]).toBe(1);
  });

  it("should return a function to reset the cached pages", () => {
    paginator.resetPages(1);
    expect(resetPages).toHaveBeenCalledWith("test", paginator.refreshPage, 1);
  });

  it("should return a function to set the page size", () => {
    paginator.setPageSize(5);
    expect(setPageSize).toHaveBeenCalledWith("test", 5);
  });

  it("should return a function to set the page sorting", () => {
    paginator.setPageSorting("test", "desc");
    expect(setPageSorting).toHaveBeenCalledWith("test", "test", "desc");
  });

  it("should return a function to update the filter", () => {
    const payload = {};
    paginator.updateFilter(payload);
    expect(updateFilter).toHaveBeenCalledWith("test", payload);
  });

  it("should return a function to trim the filter", () => {
    const payload = {};
    paginator.trimFilter(payload);
    expect(trimFilter).toHaveBeenCalledWith("test", payload);
  });
});
