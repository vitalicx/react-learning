import Immutable from "seamless-immutable";
import Types from "./../../constants/action-types";
import reducer from "./reducers";
import * as actions from "./actions";

describe("Pagination reducer", () => {
  const requestPage = page => actions.requestPage("test", null, null, false, page);

  const appState = Immutable({
    currentPage: 1,
    isLoading: false,
    filter: { title: "test" },
    pages: {},
  });

  it("should handle REQUEST_PAGE", () => {
    const newState = reducer(appState, requestPage(2));
    expect(newState.currentPage).toBe(2);
    expect(newState.isLoading).toBeTruthy();
  });

  it("should handle RECEIVE_PAGE", () => {
    const actionList = [
      requestPage(1),
      actions.receivePage("test", 1, {
        total: 5,
        result: "test",
      }),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.isLoading).toBeFalsy();
    expect(newState.pages["1"]).toMatch("test");
    expect(newState.total).toBe(5);
  });

  it("should handle RECEIVE_PAGE_FAILURE", () => {
    const actionList = [requestPage(1), actions.requestPageFailure("test", 1, "test error")];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.isLoading).toBeFalsy();
    expect(newState.error).toEqual("test error");
  });

  it("should handle RESET_PAGES", () => {
    const actionList = [
      actions.receivePage("test", 1, {
        total: 5,
        result: "test",
      }),
      {
        type: Types.RESET_PAGES,
        meta: { key: "test" },
        payload: {},
      },
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.pages).toEqual({});
  });

  it("should handle RESET_PAGES when `currentPage` exists in the action payload", () => {
    const actionList = [
      actions.receivePage("test", 1, {
        total: 5,
        result: "test",
      }),
      {
        type: Types.RESET_PAGES,
        meta: { key: "test" },
        payload: { currentPage: 1 },
      },
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.pages).toHaveProperty("1");
  });

  it("should handle SET_PAGE_SIZE", () => {
    const newState = reducer(appState, actions.setPageSize("test", 5));
    expect(newState.pageSize).toEqual(5);
  });

  it("should handle UPDATE_FILTER", () => {
    const actionList = [
      actions.updateFilter("test", { title: "test change" }),
      actions.setPageSorting("test", "field", "order"),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.filter.title).toEqual("test change");
    expect(newState.filter.sortField).toEqual("field");
    expect(newState.filter.sortOrder).toEqual("order");
  });

  it("should handle TRIM_FILTER", () => {
    const newState = reducer(appState, actions.trimFilter("test", "title"));
    expect(newState.filter.title).toBeUndefined();
  });
});
