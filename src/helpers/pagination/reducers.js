import Immutable from "seamless-immutable";
import Types from "../../constants/action-types";
import { createReducer } from "reduxsauce";
import { combineReducers } from "redux";

const PAGE_SIZE_DEFAULT = 5;

const pages = createReducer(Immutable({}), {
  [Types.RECEIVE_PAGE]: (state, { data, meta }) => state.merge({ [meta.page]: data.result }),
  [Types.RESET_PAGES]: (state, { payload: { currentPage } }) =>
    state.replace(currentPage ? { [currentPage]: state[currentPage] } : {})
});

const currentPage = createReducer(1, {
  [Types.REQUEST_PAGE]: (state, { payload: { page } }) => page,
});

const total = createReducer(0, {
  [Types.RECEIVE_PAGE]: (state, { data }) => data.total,
});

const isLoading = createReducer(true, {
  [Types.RECEIVE_PAGE]: () => false,
  [Types.REQUEST_PAGE]: (state, { meta: { fromCache } }) => !fromCache,
  [Types.REQUEST_PAGE_FAILURE]: () => false,
});

const error = createReducer({}, {
  [Types.REQUEST_PAGE]: () => ({}),
  [Types.REQUEST_PAGE_FAILURE]: (state, { error }) => error,
});

const filter = createReducer(Immutable({}), {
  [Types.UPDATE_FILTER]: (state, { payload }) => state.merge(payload),
  [Types.TRIM_FILTER]: (state, { payload }) => state.without(payload),
});

const pageSize = createReducer(PAGE_SIZE_DEFAULT, {
  [Types.SET_PAGE_SIZE]: (state, { payload: { pageSize } }) => pageSize,
});

const reducers = combineReducers({
  pages,
  currentPage,
  total,
  isLoading,
  error,
  filter,
  pageSize
});

export default reducers;
