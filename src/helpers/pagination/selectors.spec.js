import { currentPageResultsFactory } from "./selectors";

describe("Pagination selectors", () => {
  const appState = {
    movies: {
      byId: {
        1: "test",
      },
    },
    paginations: {
      movies: {
        currentPage: 1,
        pages: {
          1: [1],
        },
      },
    },
  };
  const selector = currentPageResultsFactory("movies");

  it("should provide a selector to retrieve current page results", () => {
    const movies = selector(appState);
    expect(movies).toBeInstanceOf(Array);
    expect(movies[0]).toMatch("test");
    expect(selector.recomputations()).toBe(1);
    selector(appState);
    expect(selector.recomputations()).toBe(1);
  });
});
