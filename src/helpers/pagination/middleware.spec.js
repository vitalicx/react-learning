import middleware, { getSortParams } from "./middleware";
import Types from "./../../constants/action-types";
import { asyncFetch } from "../../helpers/fetch";
import { receivePage, requestPageFailure } from "./actions";
import { mockErrorOnce } from "./../testing";

jest.mock("../../helpers/fetch");

describe("Pagination middleware", () => {
  const getState = () => ({
    paginations: {
      test: {
        currentPage: 1,
        pageSize: 5,
        filter: { title: "existing filter", sortField: "test", sortOrder: "desc" },
        pages: { 1: "test" },
      },
    },
    user: {
      data: {
        token: "test",
      },
    },
  });
  const dispatch = jest.fn();
  const next = jest.fn();

  const requestPage = (page, filter, refresh = false) => ({
    type: Types.REQUEST_PAGE,
    meta: {
      key: "test",
      endpoint: "http://test",
      normalizer: jest.fn(data => data),
      refresh,
    },
    payload: { page, pageSize: 5, path: "/path", filter },
  });

  beforeEach(() => {
    dispatch.mockClear();
    next.mockClear();
  });

  it("should execute and return the next action callback", () => {
    const action = requestPage(2);
    middleware({ dispatch, getState })(next)(action);
    expect(next).toHaveBeenCalledWith(action);
  });

  it("should create an action to request and receive a page", async () => {
    middleware({ dispatch, getState })(next)(requestPage(2));
    const thunk = dispatch.mock.calls[0][0];
    expect(thunk).toEqual(expect.any(Function));

    asyncFetch.mockReturnValueOnce({ json: jest.fn(), headers: { get: () => "1/5" } });
    await thunk(dispatch);
    expect(asyncFetch).toHaveBeenCalledWith(
      "http://test/path",
      expect.objectContaining({
        token: "test",
        query: expect.objectContaining({
          page: 2,
          pageSize: 5,
          title: "existing filter",
          "order[test]": "desc",
        }),
      })
    );
    expect(dispatch).lastCalledWith(receivePage("test", 2, { total: 5 }));

    mockErrorOnce(asyncFetch);
    await thunk(dispatch);
    expect(dispatch).lastCalledWith(requestPageFailure("test", 2, Error("")));
  });

  describe("when the page exists in cache", () => {
    it("returns the cached results and sets `fromCache` meta value to true", () => {
      middleware({ dispatch, getState })(next)(requestPage(1));
      expect(dispatch).not.toHaveBeenCalled();
      expect(next).toHaveBeenCalledWith(
        expect.objectContaining({
          meta: expect.objectContaining({
            fromCache: true,
          }),
        })
      );
    });

    it("does not return cached results if the filter object changed", () => {
      middleware({ dispatch, getState })(next)(requestPage(1, "test", true));
      expect(dispatch).toHaveBeenCalled();
      expect(next).toHaveBeenCalledWith(
        expect.objectContaining({
          meta: expect.not.objectContaining({
            fromCache: false,
          }),
        })
      );
    });
  });

  describe("when determining the sorting query parameters", () => {
    it("should return correct sort parameters for descending order", () => {
      const params = getSortParams("test", "desc");
      expect(params).toEqual({ "order[test]": "desc" });
    });
    it("should return correct sort parameters for ascending order", () => {
      const params = getSortParams("test", "ascend");
      expect(params).toEqual({ "order[test]": "asc" });
    });
    it("should not return any sort parameters if no field is provided", () => {
      const params = getSortParams();
      expect(params).toBeUndefined();
    });
  });
});
