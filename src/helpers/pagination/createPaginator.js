import reducers from "./reducers";
import { requestPage, resetPages, setPageSize, setPageSorting, updateFilter, trimFilter } from "./actions";
import { currentPageResultsFactory } from "./selectors";

const onlyForKey = (key, reducer) => (state = {}, action = {}) =>
  !!action.meta && key !== action.meta.key ? state : reducer(state, action);

const requestPageActionFactory = (key, endpoint, normalizer, refresh = false) => (page, path = "") =>
  requestPage(key, endpoint, normalizer, refresh, page, path);
const resetPagesActionFactory = (key, refreshPage) => currentPage => resetPages(key, refreshPage, currentPage);
const setPageSizeActionFactory = key => pageSize => setPageSize(key, pageSize);
const setPageSortingActionFactory = key => (sortField, sortOrder) => setPageSorting(key, sortField, sortOrder);
const updateFilterActionFactory = key => payload => updateFilter(key, payload);
const trimFilterActionFactory = key => payload => trimFilter(key, payload);

const createPaginator = (key, endpoint, normalizer) => {
  const requestPage = requestPageActionFactory(key, endpoint, normalizer);
  const refreshPage = requestPageActionFactory(key, endpoint, normalizer, true);
  const getCurrentPageResults = currentPageResultsFactory(key);
  const resetPages = resetPagesActionFactory(key, refreshPage);
  const setPageSize = setPageSizeActionFactory(key);
  const setPageSorting = setPageSortingActionFactory(key);
  const updateFilter = updateFilterActionFactory(key);
  const trimFilter = trimFilterActionFactory(key);

  return {
    reducers: onlyForKey(key, reducers),
    requestPage,
    refreshPage,
    getCurrentPageResults,
    resetPages,
    setPageSize,
    setPageSorting,
    updateFilter,
    trimFilter,
  };
};

export default createPaginator;
