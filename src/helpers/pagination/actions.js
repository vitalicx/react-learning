import Types from "./../../constants/action-types";

export const requestPage = (key, endpoint, normalizer, refresh, page, path) => ({
  type: Types.REQUEST_PAGE,
  meta: { key, endpoint, normalizer, refresh },
  payload: { page, path },
});

export const receivePage = (key, page, data) => ({
  type: Types.RECEIVE_PAGE,
  meta: { key, page },
  data,
});

export const requestPageFailure = (key, page, error) => ({
  type: Types.REQUEST_PAGE_FAILURE,
  meta: { key, page },
  error,
});

export const resetPages = (key, refreshPage, currentPage) => dispatch => {
  if (currentPage) {
    dispatch(refreshPage(currentPage));
  }
  dispatch({
    type: Types.RESET_PAGES,
    meta: { key },
    payload: { currentPage },
  });
};

export const setPageSize = (key, pageSize) => ({
  type: Types.SET_PAGE_SIZE,
  meta: { key },
  payload: { pageSize },
});

export const setPageSorting = (key, sortField, sortOrder) => ({
  type: Types.UPDATE_FILTER,
  meta: { key },
  payload: { sortField, sortOrder },
});

export const updateFilter = (key, payload) => ({
  type: Types.UPDATE_FILTER,
  meta: { key },
  payload,
});

export const trimFilter = (key, payload) => ({
  type: Types.TRIM_FILTER,
  meta: { key },
  payload,
});
