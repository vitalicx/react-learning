export const mockErrorOnce = (mockFunction, error = Error()) =>
  mockFunction.mockImplementationOnce(() => {
    throw error;
  });
