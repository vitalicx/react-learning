import { API_BASE_URL, API_DEFAULT_HEADERS } from "../config";

export async function asyncFetch(path, config) {
  try {
    const { token, body, headers, query, json = true, ...rest } = config;
    const url = new URL(API_BASE_URL + path);
    const fetchConfig = {
      headers: {
        ...API_DEFAULT_HEADERS,
        ...headers,
        ...(token && { Authorization: `Bearer ${token}` }),
      },
      ...(body && { body: JSON.stringify(body) }),
      ...rest,
    };
    if (query) {
      Object.keys(query).forEach(key => url.searchParams.append(key, query[key]));
    }
    const response = await fetch(url, fetchConfig);
    if (false === response.ok) {
      throw response;
    }
    if (204 !== response.status) {
      return json ? await response.json() : response;
    }
  } catch (e) {
    throw await e.json();
  }
}
