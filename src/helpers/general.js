import { isObject, transform } from "lodash";

/**
 * Recursively mutate an object
 * @param {Object} obj
 * @param {function} iterator
 * @returns {Object}
 */
export function deepMap(obj, iterator) {
  return transform(obj, (result, val, key) => {
    result[key] = isObject(val)
      ? deepMap(val, iterator)
      : iterator(val, key, obj);
  });
}