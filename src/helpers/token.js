import { refresh } from "../actions/user";
import jwtDecode from "jwt-decode";
import { getUserData } from "../reducers/user";

export const checkTokenExpiry = store => () => {
  const userData = getUserData(store.getState());
  if (null === userData) return;
  if (userData.expiry < new Date()) {
    store.dispatch(refresh(userData.refreshToken));
  }
};

export const decodeToken = (token, refreshToken) => {
  const decodedData = jwtDecode(token);
  return {
    token,
    refreshToken,
    name: decodedData.username,
    roles: decodedData.roles,
    expiry: new Date(decodedData.exp * 1000),
  };
};
