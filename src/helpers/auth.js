import locationHelperBuilder from "redux-auth-wrapper/history4/locationHelper";
import { connectedRouterRedirect } from "redux-auth-wrapper/history4/redirect";
import connectedAuthWrapper from "redux-auth-wrapper/connectedAuthWrapper";
import { getIsAuthenticated, getIsAuthenticating, getIsAdmin } from "../reducers/user";

const locationHelper = locationHelperBuilder({});

const userIsAuthenticatedDefaults = {
  authenticatedSelector: state => getIsAuthenticated(state),
  authenticatingSelector: state => getIsAuthenticating(state),
  wrapperDisplayName: "UserIsAuthenticated",
};

export const userIsAuthenticated = connectedAuthWrapper(userIsAuthenticatedDefaults);

export const userIsAuthenticatedRedir = connectedRouterRedirect({
  ...userIsAuthenticatedDefaults,
  redirectPath: "/login",
});

const userIsNotAuthenticatedDefaults = {
  // Want to redirect the user when they are done loading and authenticated
  authenticatedSelector: state =>
    false === getIsAuthenticated(state) &&
    false === getIsAuthenticating(state),
  wrapperDisplayName: "UserIsNotAuthenticated",
};

export const userIsNotAuthenticated = connectedAuthWrapper(userIsNotAuthenticatedDefaults);

export const userIsNotAuthenticatedRedir = connectedRouterRedirect({
  ...userIsNotAuthenticatedDefaults,
  redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || "/movies",
  allowRedirectBack: false,
});

export const userIsAdminRedir = connectedRouterRedirect({
  redirectPath: "/",
  allowRedirectBack: false,
  authenticatedSelector: state => getIsAdmin(state),
  wrapperDisplayName: "UserIsAdmin",
});
