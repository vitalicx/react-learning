import { asyncFetch } from "./fetch";
import { fetchMock } from "fetch-mock";
import { API_BASE_URL } from "../config";

describe("Async fetch helper", () => {
  let filter;

  beforeEach(() => {
    filter = `${API_BASE_URL}/test`;
  });

  afterEach(() => {
    expect(fetchMock.called(filter)).toBeTruthy();
    fetchMock.restore();
  });

  it("should handle a successful request", async () => {
    fetchMock.get(filter, JSON.stringify("test"));
    const data = await asyncFetch("/test", { method: "GET" });
    expect(data).toEqual("test");
  });

  it("should handle an unsuccessful request", async () => {
    fetchMock.get(filter, { status: 401, body: JSON.stringify("error") });
    try {
      await asyncFetch("/test", { method: "GET" });
    } catch (error) {
      expect(error).toEqual("error");
    }
    expect.assertions(2);
  });

  it("should handle a 204 no content response", async () => {
    fetchMock.get(filter, { status: 204 });
    const data = await asyncFetch("/test", { method: "GET" });
    expect(data).toBeUndefined();
  });

  it("should handle a POST request with provided body", async () => {
    fetchMock.post(filter, JSON.stringify("test"));
    const body = { title: "test" };
    const data = await asyncFetch("/test", { method: "POST", body });
    expect(data).toEqual("test");
    const fetchConfig = fetchMock.lastOptions(filter);
    expect(fetchConfig).toHaveProperty("body");
    expect(fetchConfig.body).toEqual(JSON.stringify(body));
  });

  it("should handle a provided auth token", async () => {
    fetchMock.get(filter, JSON.stringify("test"));
    const token = "test";
    const data = await asyncFetch("/test", { method: "GET", token });
    expect(data).toEqual("test");
    const fetchConfig = fetchMock.lastOptions(filter);
    expect(fetchConfig).toHaveProperty("headers");
    expect(fetchConfig.headers).toHaveProperty("Authorization");
    expect(fetchConfig.headers.Authorization).toEqual(`Bearer ${token}`);
  });

  it("should handle a provided header", async () => {
    fetchMock.get(filter, JSON.stringify("test"));
    const data = await asyncFetch("/test", { method: "GET", headers: { Accept: "text/html" } });
    expect(data).toEqual("test");
    const fetchConfig = fetchMock.lastOptions(filter);
    expect(fetchConfig).toHaveProperty("headers");
    expect(fetchConfig.headers).toHaveProperty("Accept");
    expect(fetchConfig.headers.Accept).toEqual("text/html");
  });

  describe("when query object is passed in config", () => {
    beforeEach(() => {
      filter = `${API_BASE_URL}/test?page=0`;
    });

    it("should append provided query parameters to url", async () => {
      fetchMock.get(filter, JSON.stringify("test"));
      const data = await asyncFetch("/test", { method: "GET", query: { page: 0 } });
      expect(data).toEqual("test");
    });
  });

  describe("when `json` is set to false in passed config object", () => {
    it("should return the response object directly", async () => {
      fetchMock.get(filter, JSON.stringify("test"));
      const response = await asyncFetch("/test", { method: "GET", json: false });
      expect(response).toHaveProperty("headers");
    });
  });
});
