import { withFormik } from "formik";
import { withTemplate } from "../components/TemplatedForm";

const createForm = config => fields => withFormik({
  ...config,
  handleSubmit: async (data, { props, setSubmitting, setFieldError, resetForm }) => {
    try {
      await props.onSubmit(data);
      if (true === props.resetAfterSubmit) {
        resetForm();
      }
    } catch (errors) {
      if (errors.length) {
        errors.forEach(err => {
          setFieldError(err.propertyPath, err.message);
        });
      }
    } finally {
      setSubmitting(false);
    }
  },
})(withTemplate(fields));

export default createForm;
