import jwtDecode from "jwt-decode";
import { refresh } from "../api/user";
import { checkTokenExpiry, decodeToken } from "./token";

jest.mock("../api/user");
jest.mock("jwt-decode");

describe("When refreshing an expired token", () => {
  refresh.mockImplementationOnce(token => ({ refreshToken: token }));
  const mockDispatch = jest.fn();
  const createStore = expiry => ({
    getState: () => ({
      user: {
        data: {
          expiry,
          refreshToken: "test",
        },
      },
    }),
    dispatch: mockDispatch,
  });

  beforeEach(() => {
    mockDispatch.mockClear();
  });

  it("should refresh the token if expired", () => {
    checkTokenExpiry(createStore(new Date(2018, 1, 1)))();
    expect(mockDispatch).toHaveBeenCalledWith(expect.any(Function));
  });

  it("should not refresh the token if still fresh", () => {
    checkTokenExpiry(createStore(new Date()))();
    expect(mockDispatch).not.toBeCalled();
  });

  it("should not try to refresh a token if logged out", () => {
    checkTokenExpiry({ getState: () => ({ user: { data: null } }) })();
    expect(mockDispatch).not.toBeCalled();
  });
});

describe("When decoding a token", () => {
  const date = new Date();
  jwtDecode.mockImplementationOnce(() => ({
    username: "test",
    roles: [],
    exp: date.getTime() / 1000,
  }));

  it("should decode the token", () => {
    const token = "test",
      refreshToken = "test";
    const tokenData = decodeToken(token, refreshToken);
    expect(tokenData).toMatchObject({
      token,
      refreshToken,
      name: expect.any(String),
      roles: expect.any(Array),
      expiry: date,
    });
  });
});
