import * as api from "./movies";
import { asyncFetch } from "../helpers/fetch";

jest.mock("../helpers/fetch");

describe("Movies API", () => {
  const token = "test";

  it("should request movies", async () => {
    await api.requestMovies(token);
    expect(asyncFetch).toBeCalledWith(expect.any(String), expect.objectContaining({ token }));
  });

  it("should request movies with search filter", async () => {
    await api.requestMovies(token, "search term");
    expect(asyncFetch).toBeCalledWith(
      expect.any(String),
      expect.objectContaining({
        token,
        query: { title: "search term" },
      })
    );
  });

  it("should add a movie", async () => {
    const body = { test: "test" };
    await api.addMovie(body, token);
    expect(asyncFetch).toBeCalledWith(expect.any(String), expect.objectContaining({ body, token }));
  });

  it("should delete a movie", async () => {
    await api.deleteMovie(0, token);
    expect(asyncFetch).toBeCalledWith(
      expect.stringContaining("0"),
      expect.objectContaining({ token })
    );
  });

  it("should update a movie", async () => {
    const body = { test: "test" };
    await api.updateMovie(0, body, token);
    expect(asyncFetch).toBeCalledWith(
      expect.stringContaining("0"),
      expect.objectContaining({ token, body })
    );
  });
});
