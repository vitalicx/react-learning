import * as api from "./actors";
import { asyncFetch } from "../helpers/fetch";

jest.mock("../helpers/fetch");

describe("Actors API", () => {
  const token = "test";

  it("should request actors", async () => {
    await api.requestActors(token);
    expect(asyncFetch).toBeCalledWith(expect.any(String), expect.objectContaining({ token }));
  });

  it("should request actors with search filter", async () => {
    await api.requestActors(token, "search term");
    expect(asyncFetch).toBeCalledWith(
      expect.any(String),
      expect.objectContaining({
        token,
        query: { name: "search term" },
      })
    );
  });

  it("should add an actor", async () => {
    const body = { test: "test" };
    await api.addActor(body, token);
    expect(asyncFetch).toBeCalledWith(expect.any(String), expect.objectContaining({ body, token }));
  });
});
