import { asyncFetch } from "../helpers/fetch";

export async function requestActors(token, filter) {
  return await asyncFetch("/actors", {
    method: "GET",
    token,
    ...(filter && { query: { name: filter } }),
  });
}

export async function addActor(body, token) {
  return await asyncFetch("/actors", { method: "POST", body, token });
}
