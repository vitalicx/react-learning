export function getErrorMessage(error) {
  return error.message || error.detail;
}