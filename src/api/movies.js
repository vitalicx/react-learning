import { asyncFetch } from "../helpers/fetch";

export async function requestMovies(token, filter) {
  return asyncFetch("/movies", {
    method: "GET",
    token,
    ...(filter && { query: { title: filter } }),
  });
}

export async function addMovie(body, token) {
  return asyncFetch("/movies", { method: "POST", body, token });
}

export async function deleteMovie(id, token) {
  return asyncFetch(`/movies/${id}`, { method: "DELETE", token });
}

export async function updateMovie(id, body, token) {
  return asyncFetch(`/movies/${id}`, { method: "PUT", body, token });
}
