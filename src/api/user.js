import { API_BASE_URL } from "../config";
import { asyncFetch } from "../helpers/fetch";

export async function login(body) {
  return await asyncFetch("/login_check", { method: "POST", body });
}

export async function refresh(token) {
  const response = await fetch(`${API_BASE_URL}/token/refresh?refresh_token=${token}`, {
    method: "POST",
  });
  return await response.json();
}