import { getErrorMessage } from "./error";

describe("API error helper", () => {
  it("should return error message from error object", () => {
    let error = { message: "test" };
    expect(getErrorMessage(error)).toEqual("test");
    error = { detail: "test" };
    expect(getErrorMessage(error)).toEqual("test");
  });
});
