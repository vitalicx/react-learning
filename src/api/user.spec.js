import * as api from "./user";
import { asyncFetch } from "../helpers/fetch";
import { fetchMock } from "fetch-mock";

jest.mock("../helpers/fetch");

describe("User API", () => {
  it("should login", async () => {
    const body = { test: "test" };
    await api.login(body);
    expect(asyncFetch).toBeCalledWith(expect.any(String), expect.objectContaining({ body }));
  });

  it("should refresh token", async () => {
    fetchMock.post("end:token", JSON.stringify("test"));
    const data = await api.refresh("token");
    expect(data).toEqual("test");
  });
});
