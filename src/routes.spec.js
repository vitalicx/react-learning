import React from "react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router";
import { mount } from "enzyme";
import routes from "./routes";

import Home from "./Home";
import MoviesContainer from "./containers/MoviesContainer";
import AdminComponent from "./components/Admin";
import LoginContainer from "./containers/LoginContainer";

jest.mock("./Home", () => jest.fn(() => null));
jest.mock("./containers/MoviesContainer", () => jest.fn(() => null));
jest.mock("./components/Admin", () => jest.fn(() => null));
jest.mock("./containers/LoginContainer", () => jest.fn(() => null));

describe("routes", () => {
  let storeData;
  const renderPath = path =>
    mount(
      <Provider store={configureMockStore()(storeData)}>
        <MemoryRouter initialEntries={[path]} initialIndex={0}>
          {routes}
        </MemoryRouter>
      </Provider>
    );

  beforeEach(() => {
    storeData = {
      user: {
        data: null,
        isLoggingIn: false,
      },
      notifications: [],
      movies: { movies: [] },
    };
  });

  describe("when logged out", () => {
    it("renders home page", () => {
      const wrap = renderPath("/");
      expect(wrap.find(Home).exists()).toBeTruthy();
    });

    it("renders login page", () => {
      const wrap = renderPath("/login");
      expect(wrap.find(LoginContainer).exists()).toBeTruthy();
    });

    it("redirects to login page when accessing movies page", () => {
      const wrap = renderPath("/movies");
      expect(wrap.find(LoginContainer).exists()).toBeTruthy();
      expect(wrap.find(MoviesContainer).exists()).toBeFalsy();
    });

    it("redirects to login page when accessing admin page", () => {
      const wrap = renderPath("/admin");
      expect(wrap.find(LoginContainer).exists()).toBeTruthy();
      expect(wrap.find(AdminComponent).exists()).toBeFalsy();
    });
  });

  describe("when logged in as a user", () => {
    beforeEach(() => {
      storeData.user.data = { roles: [] };
    });

    it("renders movies page", () => {
      const wrap = renderPath("/movies");
      expect(wrap.find(MoviesContainer).exists()).toBeTruthy();
    });

    it("redirects to login page when accessing admin page", () => {
      const wrap = renderPath("/admin");
      expect(wrap.find(Home).exists()).toBeTruthy();
      expect(wrap.find(AdminComponent).exists()).toBeFalsy();
    });
  });

  describe("when logged in as an admin", () => {
    beforeEach(() => {
      storeData.user.data = { roles: ["ROLE_ADMIN"] };
    });

    it("renders admin page", () => {
      const wrap = renderPath("/admin");
      expect(wrap.find(AdminComponent).exists()).toBeTruthy();
    });
  });
});
