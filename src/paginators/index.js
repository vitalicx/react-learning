import createPaginator from "../helpers/pagination/createPaginator";
import moviesNormalizer from "../actions/moviesNormalizer";

export const moviesPaginator = createPaginator("movies", "/movies", moviesNormalizer);