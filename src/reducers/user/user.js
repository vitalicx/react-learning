import Types from "../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { combineReducers } from "redux";
import { isEmpty } from "lodash";

export const getUser = state => state.user;
export const getUserData = state => state.user.data;
export const getToken = state => getUserData(state).token;
export const getIsAuthenticated = state => false === isEmpty(getUserData(state));
export const getIsAuthenticating = state => state.user.isLoggingIn;
export const getIsAdmin = state => getUserData(state).roles.includes("ROLE_ADMIN");

const data = createReducer(Immutable({}), {
  [Types.USER_LOGGED_IN]: (state, { data }) => state.merge(data),
  [Types.USER_LOGGED_OUT]: () => Immutable({}),
  [Types.USER_REFRESHED_TOKEN]: (state, { tokens }) => state.merge(tokens),
});

const isLoggingIn = createReducer(false, {
  [Types.USER_LOGGING_IN]: () => true,
  [Types.USER_LOGGED_IN]: () => false,
});

export default combineReducers({
  data,
  isLoggingIn,
});
