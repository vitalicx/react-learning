import Immutable from "seamless-immutable";
import reducer from "./user";
import { loginSuccess, loggingIn, logout, userRefreshedToken } from "../../actions/user";

describe("User reducer", () => {
  const loggedInState = {
    data: Immutable({ name: "user", token: "token" }),
    isLoggingIn: false,
  };

  it("should provide a default state", () => {
    const state = reducer(undefined, { type: "unknown" });
    expect(state).toHaveProperty("data");
    expect(state).toHaveProperty("isLoggingIn");
  });

  it("should handle USER_LOGGING_IN", () => {
    const newState = reducer(undefined, loggingIn());
    expect(newState.isLoggingIn).toBeTruthy();
  });

  it("should handle USER_LOGGED_IN", () => {
    const loginData = { name: "test" };
    const actions = [loggingIn(), loginSuccess(loginData)];
    const finalState = actions.reduce(reducer, undefined);
    expect(finalState.data).toEqual(loginData);
    expect(finalState.isLoggingIn).toBeFalsy();
  });

  it("should handle USER_LOGGED_OUT", () => {
    const newState = reducer(loggedInState, logout());
    expect(newState.data).toEqual({});
  });

  it("should handle USER_REFRESHED_TOKEN", () => {
    const newState = reducer(
      loggedInState,
      userRefreshedToken({ token: "test", refreshToken: "test" })
    );
    expect(newState.data.token).toBe("test");
    expect(newState.data.refreshToken).toBe("test");
  });
});
