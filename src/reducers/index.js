import { combineReducers } from "redux";
import { reducer as notifications } from "react-notification-system-redux";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";
import {
  seamlessImmutableReconciler,
  seamlessImmutableTransformCreator,
} from "redux-persist-seamless-immutable";
import dateTransformer from "./transformers/dateTransformer";
import movies from "./movies";
import actors from "./actors";
import user from "./user";
import ui from "./ui";
import { moviesPaginator } from "./../paginators/index";

const persistConfig = {
  key: "user",
  storage,
  stateReconciler: seamlessImmutableReconciler,
  whitelist: ["data"],
  transforms: [dateTransformer, seamlessImmutableTransformCreator({})],
};

const rootReducer = combineReducers({
  notifications,
  movies,
  actors,
  user: persistReducer(persistConfig, user),
  ui,
  paginations: combineReducers({
    movies: moviesPaginator.reducers
  })
});

export default rootReducer;
