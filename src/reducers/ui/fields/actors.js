import Types from "../../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { combineReducers } from "redux";
import { createSelector } from "reselect";
import { getEntities } from "../../../actions/actorsNormalizer";

export const getActors = createSelector(
  state => state.ui.fields.actors.byId,
  actors => Object.keys(actors).map(key => actors[key])
);
export const getIsLoading = state => state.ui.fields.actors.isLoading;
export const getError = state => state.ui.fields.actors.error;

const replaceActors = (state, { data }) => state.replace(getEntities(data));

const byId = createReducer(Immutable({}), {
  [Types.UI_SELECT_ACTORS_SEARCH_SUCCESS]: replaceActors,
  [Types.UI_SELECT_ACTORS_SEARCH_REQUEST]: state => state.replace({}),
});

const isLoading = createReducer(false, {
  [Types.UI_SELECT_ACTORS_SEARCH_REQUEST]: () => true,
  [Types.UI_SELECT_ACTORS_SEARCH_SUCCESS]: () => false,
  [Types.UI_SELECT_ACTORS_SEARCH_FAILURE]: () => false,
});

const error = createReducer({}, {
  [Types.UI_SELECT_ACTORS_SEARCH_REQUEST]: () => ({}),
  [Types.UI_SELECT_ACTORS_SEARCH_FAILURE]: (state, { error }) => error,
});

export default combineReducers({
  byId,
  isLoading,
  error,
});

