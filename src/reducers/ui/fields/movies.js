import Types from "../../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { combineReducers } from "redux";
import { createSelector } from "reselect";
import { getEntities } from "../../../actions/moviesNormalizer";

export const getMovies = createSelector(
  state => state.ui.fields.movies.byId,
  movies => Object.keys(movies).map(key => movies[key])
);
export const getIsLoading = state => state.ui.fields.movies.isLoading;
export const getError = state => state.ui.fields.movies.error;

const replaceMovies = (state, { data }) => state.replace(getEntities(data));

const byId = createReducer(Immutable({}), {
  [Types.UI_SELECT_MOVIES_SEARCH_SUCCESS]: replaceMovies,
  [Types.UI_SELECT_MOVIES_SEARCH_REQUEST]: state => state.replace({}),
});

const isLoading = createReducer(false, {
  [Types.UI_SELECT_MOVIES_SEARCH_REQUEST]: () => true,
  [Types.UI_SELECT_MOVIES_SEARCH_SUCCESS]: () => false,
  [Types.UI_SELECT_MOVIES_SEARCH_FAILURE]: () => false,
});

const error = createReducer({}, {
  [Types.UI_SELECT_MOVIES_SEARCH_REQUEST]: () => ({}),
  [Types.UI_SELECT_MOVIES_SEARCH_FAILURE]: (state, { error }) => error,
});

export default combineReducers({
  byId,
  isLoading,
  error,
});

