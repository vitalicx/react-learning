import Immutable from "seamless-immutable";
import reducer, { getMovies } from "./movies";
import * as actions from "../../../actions/fields/movies";

describe("Movies reducer", () => {
  const createEntitiesObject = movies => ({
    entities: {
      movies: movies.reduce((obj, item) => {
        obj[item.id] = item;
        return obj;
      }, {}),
    },
  });

  const appState = {
    byId: Immutable({ 0: { id: 0, title: "example" } }),
    isLoading: false,
    error: null,
  };

  it("should provide a default state", () => {
    const state = reducer(undefined, { type: "unknown" });
    expect(state).toHaveProperty("byId");
    expect(state).toHaveProperty("isLoading");
    expect(state).toHaveProperty("error");
  });

  it("has an movies selector which returns an array of movies", () => {
    const state = { ui: { fields: { movies: { byId: { 0: { title: "test" } } } } } };
    const movies = getMovies(state);
    expect(movies).toBeInstanceOf(Array);
    expect(movies[0].title).toBe("test");
  });

  it("has an movies selector which caches results", () => {
    getMovies.resetRecomputations();
    const state = { ui: { fields: { movies: { byId: { 0: { title: "test" } } } } } };
    getMovies(state);
    expect(getMovies.recomputations()).toBe(1);
    getMovies(state);
    expect(getMovies.recomputations()).toBe(1);
  });

  it("should handle UI_SELECT_MOVIES_SEARCH_REQUEST", () => {
    const newState = reducer(appState, actions.requestingMovies());
    expect(newState.byId).not.toHaveProperty("0");
    expect(newState.isLoading).toBeTruthy();
    expect(newState.error).toEqual({});
  });

  it("should handle UI_SELECT_MOVIES_SEARCH_FAILURE", () => {
    const actionList = [actions.requestingMovies(), actions.requestMoviesFailure("test")];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.isLoading).toBeFalsy();
    expect(newState.error).toEqual("test");
  });

  it("should handle UI_SELECT_MOVIES_SEARCH_SUCCESS", () => {
    const actionList = [
      actions.requestingMovies(),
      actions.receivedMovies(createEntitiesObject([{ id: 0, title: "test" }])),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.isLoading).toBeFalsy();
    expect(newState.byId["0"].title).toEqual("test");
  });

  it("should handle UI_SELECT_MOVIES_SEARCH_SUCCESS when no entities returned", () => {
    const newState = reducer(appState, actions.receivedMovies({ entities: {}, result: [] }));
    expect(newState.byId).toEqual({});
  });
});
