import Immutable from "seamless-immutable";
import reducer, { getActors } from "./actors";
import * as actions from "../../../actions/fields/actors";

describe("Actors reducer", () => {
  const createEntitiesObject = actors => ({
    entities: {
      actors: actors.reduce((obj, item) => {
        obj[item.id] = item;
        return obj;
      }, {}),
    },
  });

  const appState = {
    byId: Immutable({ 0: { id: 0, name: "example" } }),
    isLoading: false,
    error: null,
  };

  it("should provide a default state", () => {
    const state = reducer(undefined, { type: "unknown" });
    expect(state).toHaveProperty("byId");
    expect(state).toHaveProperty("isLoading");
    expect(state).toHaveProperty("error");
  });

  it("has an actors selector which returns an array of actors", () => {
    const state = { ui: { fields: { actors: { byId: { 0: { name: "test" } } } } } };
    const actors = getActors(state);
    expect(actors).toBeInstanceOf(Array);
    expect(actors[0].name).toBe("test");
  });

  it("has an actors selector which caches results", () => {
    getActors.resetRecomputations();
    const state = { ui: { fields: { actors: { byId: { 0: { name: "test" } } } } } };
    getActors(state);
    expect(getActors.recomputations()).toBe(1);
    getActors(state);
    expect(getActors.recomputations()).toBe(1);
  });

  it("should handle UI_SELECT_ACTORS_SEARCH_REQUEST", () => {
    const newState = reducer(appState, actions.requestingActors());
    expect(newState.byId).not.toHaveProperty("0");
    expect(newState.isLoading).toBeTruthy();
    expect(newState.error).toEqual({});
  });

  it("should handle UI_SELECT_ACTORS_SEARCH_FAILURE", () => {
    const actionList = [actions.requestingActors(), actions.requestActorsFailure("test")];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.isLoading).toBeFalsy();
    expect(newState.error).toEqual("test");
  });

  it("should handle UI_SELECT_ACTORS_SEARCH_SUCCESS", () => {
    const actionList = [
      actions.requestingActors(),
      actions.receivedActors(createEntitiesObject([{ id: 0, name: "test" }])),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.isLoading).toBeFalsy();
    expect(newState.byId["0"].name).toEqual("test");
  });

  it("should handle UI_SELECT_ACTORS_SEARCH_SUCCESS when no entities returned", () => {
    const newState = reducer(appState, actions.receivedActors({ entities: {}, result: [] }));
    expect(newState.byId).toEqual({});
  });
});
