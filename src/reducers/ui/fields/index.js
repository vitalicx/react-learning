import { combineReducers } from "redux";
import actors from "./actors";
import movies from "./movies";

export default combineReducers({
  actors,
  movies,
});
