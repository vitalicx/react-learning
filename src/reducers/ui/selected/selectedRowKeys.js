import Types from "../../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";

export const getSelectedRowKeys = (state, key) => state.ui.selectedRowKeys[key];

const updateKeys = (state, { stateKey, keys }) => state.set(stateKey, keys);

const selectedRowKeys = createReducer(Immutable({ movies: [] }), {
  [Types.UI_SELECTED_ROWS_CHANGED]: updateKeys,
  [Types.DELETE_MOVIE_SUCCESS]: (state, { id }) =>
    state.set("movies", state.movies.filter(movie => movie !== id)),
});

export default selectedRowKeys;
