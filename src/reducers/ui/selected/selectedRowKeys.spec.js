import Immutable from "seamless-immutable";
import reducer from "./selectedRowKeys";
import { setSelectedRows } from "../../../actions/selectedRows";
import { deleteMovieSuccess } from "../../../actions/movies";

describe("selectedRowKeys reducer", () => {
  const appState = Immutable({ movies: [1] });

  it("should handle UI_SELECTED_ROWS_CHANGED", () => {
    const newState = reducer(appState, setSelectedRows("movies", [1, 2]));
    expect(newState.movies).toEqual([1, 2]);
  });

  it("should handle DELETE_MOVIE_SUCCESS", () => {
    const newState = reducer(appState, deleteMovieSuccess(1));
    expect(newState.movies).toEqual([]);
  });
});
