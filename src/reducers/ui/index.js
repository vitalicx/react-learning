import { combineReducers } from "redux";
import fields from "./fields";
import selectedRowKeys from "./selected/selectedRowKeys";

export default combineReducers({
  fields,
  selectedRowKeys,
});
