import { createTransform } from "redux-persist";
import { deepMap } from "../../helpers/general";

export default createTransform(
  inboundState => inboundState,
  outboundState => deepMap(outboundState, val => (isNaN(Date.parse(val)) ? val : new Date(val)))
);