import dateTransformer from "./dateTransformer";

describe("Date transformer", () => {
  const state = {
    regularString: "test",
    dateString: "2018-12-01T01:12:41.000Z",
    nestedData: {
      nestedDateString: "2018-12-01T01:12:41.000Z",
    },
  };

  it("should convert date strings to Date objects", () => {
    const transformed = dateTransformer.out(state);
    expect(transformed.dateString).toBeInstanceOf(Date);
    expect(transformed.nestedData.nestedDateString).toBeInstanceOf(Date);
  });

  it("should leave inbound state untouched", () => {
    const transformed = dateTransformer.in(state);
    expect(transformed).toBe(state);
  });
});
