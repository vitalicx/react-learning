import Types from "../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { combineReducers } from "redux";
import { getEntities } from "../../actions/actorsNormalizer";

export const getActorsForMovie = (state, movie) => movie.actors.map(id => state.actors.byId[id]);

const handleActors = (state, { data }) => state.merge(getEntities(data));

const byId = createReducer(Immutable({}), {
  [Types.RECEIVE_PAGE]: handleActors,
  [Types.ADD_MOVIE]: handleActors,
  [Types.ADD_ACTOR]: handleActors,
});

export default combineReducers({
  byId,
});
