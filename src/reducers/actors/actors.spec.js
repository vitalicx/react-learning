import Immutable from "seamless-immutable";
import reducer, { getActorsForMovie } from "./actors";
import * as movieActions from "../../actions/movies";
import * as actorActions from "../../actions/actors";
import { receivePage } from "../../helpers/pagination/actions";

describe("Actors reducer", () => {
  const createEntitiesObject = actors => ({
    entities: {
      actors: actors.reduce((obj, item) => {
        obj[item.id] = item;
        return obj;
      }, {}),
    },
  });

  const appState = {
    byId: Immutable({ 0: { id: 0, name: "example" } }),
  };

  it("should provide a default state", () => {
    const state = reducer(undefined, { type: "unknown" });
    expect(state).toHaveProperty("byId");
  });

  it("has a selector which returns an array of actors for a movie", () => {
    const movie = { actors: [0] };
    const state = { actors: { byId: { 0: { name: "test" } } } };
    const actors = getActorsForMovie(state, movie);
    expect(actors).toBeInstanceOf(Array);
    expect(actors[0].name).toBe("test");
  });

  it("should handle RECEIVE_PAGE", () => {
    const newState = reducer(appState, receivePage("test", 1, createEntitiesObject([{ id: 1, name: "test" }])));
    expect(newState.byId["0"].name).toEqual("example");
    expect(newState.byId["1"].name).toEqual("test");
  });

  it("should handle ADD_MOVIE", () => {
    const newState = reducer(appState, movieActions.addMovieSuccess(createEntitiesObject([{ id: 1, name: "test" }])));
    expect(newState.byId["0"].name).toEqual("example");
    expect(newState.byId["1"].name).toEqual("test");
  });

  it("should handle ADD_ACTOR", () => {
    const newState = reducer(appState, actorActions.addActorSuccess(createEntitiesObject([{ id: 1, name: "test" }])));
    expect(newState.byId["0"].name).toEqual("example");
    expect(newState.byId["1"].name).toEqual("test");
  });
});
