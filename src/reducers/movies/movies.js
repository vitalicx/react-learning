import Types from "../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { combineReducers } from "redux";
import actions from "./actions/actions";
import { getEntities } from "../../actions/moviesNormalizer";

export const getMoviesById = state => state.movies.byId;

const handleMovies = (state, { data }) => state.merge(getEntities(data));

const byId = createReducer(Immutable({}), {
  [Types.RECEIVE_PAGE]: handleMovies,
  [Types.ADD_MOVIE]: handleMovies,
  [Types.INCREASE_COUNT_SUCCESS]: handleMovies,
  [Types.DELETE_ACTOR_SUCCESS]: handleMovies,
  [Types.DELETE_MOVIE_SUCCESS]: (state, { id }) => state.without(id),
  [Types.ADD_ACTOR]: handleMovies,
});

export default combineReducers({
  byId,
  actions,
});
