import Types from "../../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { addId, removeId } from "./update";

export default createReducer(Immutable([]), {
  [Types.DELETE_MOVIE_REQUEST]: addId,
  [Types.DELETE_MOVIE_SUCCESS]: removeId,
  [Types.DELETE_MOVIE_FAILURE]: removeId,
});
