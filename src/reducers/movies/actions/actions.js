import { combineReducers } from "redux";
import updateMovie from "./update";
import deleteMovie from "./delete";
import deleteActor from "./deleteActor";
import { isEmpty } from "lodash";

export const getActions = state => state.movies.actions;
export const getIsIncreasing = (actions, id) => actions.increaseCount.includes(id);
export const getIsDeleting = (actions, id) => actions.deleteMovie.includes(id);

export const getIsDeletingActor = (actions, movieId, actorId) =>
  !!actions.deleteActor[movieId] && actions.deleteActor[movieId].includes(actorId);

export const getActionsPending = actions =>
  !!Object.keys(actions).find(key => !isEmpty(actions[key]));

export default combineReducers({
  increaseCount: updateMovie("INCREASE_COUNT"),
  deleteMovie,
  deleteActor,
});
