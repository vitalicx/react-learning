import Types from "../../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { getEntityId } from "../../../actions/moviesNormalizer";

const addId = (state, { id }) => state.concat(id);
const remove = (state, { data }) => removeId(state, { id: getEntityId(data) });
const removeId = (state, { id }) => state.filter(movieId => movieId !== id);

export { addId, remove, removeId };

export default key =>
  createReducer(Immutable([]), {
    [Types[`${key}_REQUEST`]]: addId,
    [Types[`${key}_SUCCESS`]]: remove,
    [Types[`${key}_FAILURE`]]: removeId,
  });
