import Immutable from "seamless-immutable";
import reducer from "./actions";
import * as actions from "../../../actions/movies";

describe("Actions reducer", () => {
  const appState = {
    increaseCount: Immutable(),
    deleteMovie: Immutable(),
    deleteActor: Immutable(),
  };

  it("should provide a default state", () => {
    const state = reducer(undefined, { type: "unknown" });
    expect(state).toHaveProperty("increaseCount");
    expect(state).toHaveProperty("deleteMovie");
    expect(state).toHaveProperty("deleteActor");
  });

  it("should handle INCREASE_COUNT_REQUEST", () => {
    const newState = reducer(appState, actions.increaseCountRequest(1));
    expect(newState.increaseCount.includes(1)).toBeTruthy();
  });

  it("should handle INCREASE_COUNT_FAILURE", () => {
    const actionList = [actions.increaseCountRequest(1), actions.increaseCountFailure("test", 1)];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.increaseCount.includes(1)).toBeFalsy();
  });

  it("should handle INCREASE_COUNT_SUCCESS", () => {
    const actionList = [
      actions.increaseCountRequest(1),
      actions.increaseCountSuccess({ result: 1 }),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.increaseCount.includes(1)).toBeFalsy();
  });

  it("should handle DELETE_MOVIE_REQUEST", () => {
    const newState = reducer(appState, actions.deleteMovieRequest(1));
    expect(newState.deleteMovie.includes(1)).toBeTruthy();
  });

  it("should handle DELETE_MOVIE_FAILURE", () => {
    const actionList = [actions.deleteMovieRequest(1), actions.deleteMovieFailure("test", 1)];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.deleteMovie.includes(1)).toBeFalsy();
  });

  it("should handle DELETE_MOVIE_SUCCESS", () => {
    const actionList = [actions.deleteMovieRequest(1), actions.deleteMovieSuccess(1)];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.deleteMovie.includes(1)).toBeFalsy();
  });

  it("should handle DELETE_ACTOR_REQUEST", () => {
    const newState = reducer(appState, actions.deleteActorRequest(1, 2));
    expect(newState.deleteActor).toHaveProperty("1");
    expect(newState.deleteActor[1].includes(2)).toBeTruthy();
  });

  it("should handle DELETE_ACTOR_REQUEST (multiple)", () => {
    const actionList = [actions.deleteActorRequest(1, 2), actions.deleteActorRequest(1, 3)];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.deleteActor).toHaveProperty("1");
    expect(newState.deleteActor[1].includes(2)).toBeTruthy();
    expect(newState.deleteActor[1].includes(3)).toBeTruthy();
  });

  it("should handle DELETE_ACTOR_FAILURE", () => {
    const actionList = [
      actions.deleteActorRequest(1, 2),
      actions.deleteActorRequest(1, 3),
      actions.deleteActorFailure("test", 1, 2),
      actions.deleteActorFailure("test", 1, 3),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.deleteActor).not.toHaveProperty("1");
  });

  it("should handle DELETE_ACTOR_SUCCESS", () => {
    const actionList = [
      actions.deleteActorRequest(1, 2),
      actions.deleteActorSuccess({ result: 1 }, 2),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.deleteActor).not.toHaveProperty("1");
  });
});
