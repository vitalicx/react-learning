import Types from "../../../constants/action-types";
import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import { getEntityId } from "../../../actions/moviesNormalizer";

const addId = (state, { movieId, actorId }) =>
  state.merge({ [movieId]: (state[movieId] || []).concat(actorId) });

const remove = (state, { data, actorId }) =>
  removeId(state, { movieId: getEntityId(data), actorId });

const removeId = (state, { movieId, actorId }) => {
  const actors = state[movieId];
  return actors.length > 1
    ? state.merge({ [movieId]: actors.filter(id => id !== actorId) })
    : state.without(movieId);
};

export default createReducer(Immutable({}), {
  [Types.DELETE_ACTOR_REQUEST]: addId,
  [Types.DELETE_ACTOR_SUCCESS]: remove,
  [Types.DELETE_ACTOR_FAILURE]: removeId,
});
