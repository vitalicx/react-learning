import Immutable from "seamless-immutable";
import reducer from "../movies";
import * as movieActions from "../../actions/movies";
import * as actorActions from "../../actions/actors";
import { receivePage } from "../../helpers/pagination/actions";

describe("Movies reducer", () => {
  const createEntitiesObject = movies => ({
    result: 0,
    entities: {
      movies: movies.reduce((obj, item) => {
        obj[item.id] = item;
        return obj;
      }, {}),
    },
  });

  const appState = {
    byId: Immutable({
      0: { id: 0, title: "example", trackCount: 0, actors: [{ id: 0 }] },
    }),
  };

  it("should provide a default state", () => {
    const state = reducer(undefined, { type: "unknown" });
    expect(state).toHaveProperty("byId");
    expect(state).toHaveProperty("actions");
  });

  it("should handle RECEIVE_PAGE", () => {
    const newState = reducer(
      appState,
      receivePage("test", 1, createEntitiesObject([{ id: 1, title: "test" }]))
    );
    expect(newState.byId[0].title).toEqual("example");
    expect(newState.byId[1].title).toEqual("test");
  });

  it("should handle ADD_MOVIE", () => {
    const newState = reducer(
      appState,
      movieActions.addMovieSuccess(createEntitiesObject([{ id: 1, title: "test" }]))
    );
    expect(newState.byId[0].title).toEqual("example");
    expect(newState.byId[1].title).toEqual("test");
  });

  it("should handle DELETE_MOVIE_SUCESS", () => {
    const newState = reducer(appState, movieActions.deleteMovieSuccess(0));
    expect(newState.byId).toEqual({});
  });

  it("should handle INCREASE_COUNT_SUCCESS", () => {
    const newState = reducer(
      appState,
      movieActions.increaseCountSuccess(
        createEntitiesObject([{ id: 1, title: "test", trackCount: 1 }])
      )
    );
    expect(newState.byId[0].trackCount).toBe(0);
    expect(newState.byId[1].trackCount).toBe(1);
  });

  it("should handle DELETE_ACTOR_SUCCESS", () => {
    const actionList = [
      movieActions.deleteActorRequest(0),
      movieActions.deleteActorSuccess(createEntitiesObject([{ id: 0, actors: [] }])),
    ];
    const newState = actionList.reduce(reducer, appState);
    expect(newState.byId[0].actors.length).toBe(0);
  });

  it("should handle ADD_ACTOR", () => {
    const newState = reducer(
      appState,
      actorActions.addActorSuccess(createEntitiesObject([{ id: 1, title: "test" }]))
    );
    expect(newState.byId[0].title).toEqual("example");
    expect(newState.byId[1].title).toEqual("test");
  });
});
