import { applyMiddleware, compose, createStore } from "redux";
import { reduxBatch } from "@manaflair/redux-batch";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistStore } from "redux-persist";
import createSagaMiddleware from "redux-saga";
import thunk from "redux-thunk";
import { checkTokenExpiry } from "../helpers/token";
import rootReducer from "../reducers/index";
import rootSaga from "../sagas/rootSaga";
import paginatorMiddleware from "./../helpers/pagination/middleware";

export const buildStore = (reducer, saga) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    reducer,
    composeWithDevTools(
      compose(
        reduxBatch,
        applyMiddleware(paginatorMiddleware, thunk, sagaMiddleware),
        reduxBatch
      )
    )
  );
  sagaMiddleware.run(saga);
  return store;
};

const store = buildStore(rootReducer, rootSaga);

persistStore(store, null, checkTokenExpiry(store));

export default store;
