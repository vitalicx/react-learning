import { buildStore } from "./index";
import { takeEvery } from "redux-saga/effects";

describe("Redux store", () => {
  const reducer = jest.fn();
  const sagaSpy = jest.fn();
  const store = buildStore(reducer, function* saga() {
    yield takeEvery("TEST_ONE", sagaSpy);
  });

  beforeEach(() => {
    reducer.mockClear();
    sagaSpy.mockClear();
  });

  it("handles thunks", () => {
    const action = jest.fn();
    store.dispatch(() => action());
    expect(action).toHaveBeenCalled();
  });

  it("handles batched actions", () => {
    store.dispatch([{ type: "TEST_ONE" }, { type: "TEST_TWO" }]);
    expect(reducer).toHaveBeenCalledWith(undefined, { type: "TEST_ONE" });
    expect(reducer).toHaveBeenLastCalledWith(undefined, { type: "TEST_TWO" });
  });

  it("handles sagas", () => {
    store.dispatch({ type: "TEST_ONE" });
    expect(sagaSpy).toHaveBeenCalled();
  });
});
