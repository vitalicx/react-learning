import React from "react";
import { shallow } from "enzyme";
import { App, mapStateToProps } from "./App";
import Notifications from "react-notification-system-redux";
import reducer from "./reducers/index";

describe("<App />", () => {
  const wrap = shallow(<App notifications={[]} logout={jest.fn()} user={{}} />);

  it("renders a `Navigation` component", () => {
    expect(wrap.find("withRouter(Navigation)").exists()).toBeTruthy();
  });

  it("renders routes", () => {
    expect(wrap.find({ path: "/movies" }).exists()).toBeTruthy();
  });

  it("renders a `Notifications` component", () => {
    expect(wrap.find(Notifications).exists()).toBeTruthy();
  });

  it("should pass correct props from state", () => {
    const props = mapStateToProps(reducer(undefined, { type: "unknown" }));
    expect(props).toMatchSnapshot();
  });
});
