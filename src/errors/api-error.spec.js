import ApiError from "./api-error";

describe("Api error", () => {
  it("creates a valid Error object", () => {
    const error = new ApiError("test error");
    expect(error.violations[0]).toBe("test error");
    expect(error.name).toBe("ApiError");
  });
});
