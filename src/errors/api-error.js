const message = "An error occurred during an API call.";

export default class ApiError extends Error {
  constructor(...violations) {
    super(message);
    this.violations = violations;
    this.stack = new Error(message).stack;
    this.name = "ApiError";
  }
}
