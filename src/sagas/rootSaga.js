import { error, success } from "react-notification-system-redux";
import { all, call, delay, put, race, select, takeEvery } from "redux-saga/effects";
import * as actions from "../actions/movies";
import { normalizeMovies } from "../actions/normalizer";
import * as api from "../api/movies";
import Types from "../constants/action-types";
import { getCurrentPage } from "../helpers/pagination/selectors";
import { getErrorMessage } from "./../api/error";
import { moviesPaginator } from "./../paginators/index";
import { getMoviesById } from "./../reducers/movies/movies";
import { filterIds } from "../constants/filterTypes";

const { getCurrentPageResults, resetPages, updateFilter, trimFilter } = moviesPaginator;

export function* increaseCountMultiple({ ids, token }) {
  try {
    yield put(ids.map(id => actions.increaseCountRequest(id)));
    const byId = yield select(getMoviesById);
    const results = yield all(ids.map(id => call(actions.doIncreaseCount, byId[id], token)));
    yield put(results.map(movie => actions.increaseCountSuccess(normalizeMovies(movie))));
    yield put(success({ title: `Updated ${ids.length} movies successfully` }));
  } catch (e) {
    yield put(ids.map(id => actions.increaseCountFailure(e, id)));
    yield put(error({ title: getErrorMessage(e) }));
  }
}

export function* notifySuccess(message) {
  let successNotification;

  const notificationPromise = new Promise(undoClicked => {
    successNotification = success({
      title: message,
      action: {
        label: "Undo",
        callback: () => undoClicked(true),
      },
    });
  });

  yield put(successNotification);

  return notificationPromise;
}

export function* deleteMovieMultiple({ ids, token }) {
  try {
    yield put(ids.map(id => actions.deleteMovieRequest(id)));
    yield delay(250);
    yield put(ids.map(id => actions.deleteMovieSuccess(id)));

    const notificationPromise = yield call(
      notifySuccess,
      `Deleted ${ids.length} movies successfully`
    );

    // Store current page *prior* to potential post-deletion change
    const currentPage = yield select(getCurrentPage, "movies");

    yield put(updateFilter({ [filterIds]: ids.join(",") }));
    yield put(actions.deleteMoviesCompleted());
    yield put(trimFilter(filterIds));

    const [undo] = yield race([notificationPromise, delay(5000)]);

    if (undo) {
      yield put(resetPages(currentPage));
    } else {
      yield all(ids.map(id => call(api.deleteMovie, id, token)));
    }
  } catch (e) {
    yield put(ids.map(id => actions.deleteMovieFailure(e, id)));
    yield put(error({ title: getErrorMessage(e) }));
  }
}

export function* deleteMoviesCompleted() {
  let currentPage = yield select(getCurrentPage, "movies");
  const movies = yield select(getCurrentPageResults);
  currentPage = currentPage > 1 && 0 === movies.length ? currentPage - 1 : currentPage;
  yield put(resetPages(currentPage));
}

export function* watchIncreaseCountMultiple() {
  yield takeEvery(Types.INCREASE_COUNT_MULTIPLE, increaseCountMultiple);
}

export function* watchDeleteMovieMultiple() {
  yield takeEvery(Types.DELETE_MOVIE_MULTIPLE, deleteMovieMultiple);
}

export function* watchDeleteMoviesCompleted() {
  yield takeEvery(Types.DELETE_MOVIES_COMPLETED, deleteMoviesCompleted);
}

export default function* rootSaga() {
  yield all([
    watchIncreaseCountMultiple(),
    watchDeleteMovieMultiple(),
    watchDeleteMoviesCompleted(),
  ]);
}
