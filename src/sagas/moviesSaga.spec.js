import { expectSaga } from "redux-saga-test-plan";
import * as matchers from "redux-saga-test-plan/matchers";
import { throwError } from "redux-saga-test-plan/providers";
import { put, select } from "redux-saga/effects";
import * as actions from "../actions/movies";
import * as api from "../api/movies";
import { filterIds } from "../constants/filterTypes";
import * as paginationActions from "../helpers/pagination/actions";
import { getCurrentPage } from "../helpers/pagination/selectors";
import { moviesPaginator } from "./../paginators";
import {
  deleteMovieMultiple as deleteMultipleSaga,
  deleteMoviesCompleted,
  increaseCountMultiple as increaseMultipleSaga,
  notifySuccess,
} from "./rootSaga";

jest.mock("../actions/normalizer", () => ({
  normalizeMovies: movies => movies,
}));

// Mock legacy thunk
paginationActions.resetPages = jest.fn((...args) => ({ ...args }));

const { getCurrentPageResults, resetPages, updateFilter, trimFilter } = moviesPaginator;

describe("when increasing track count of multiple movies", () => {
  const movies = { 1: { id: 1 }, 2: { id: 2 } };
  const error = new Error("test error");
  const requestActions = Object.keys(movies).map(id => actions.increaseCountRequest(id));
  const successActions = Object.values(movies).map(movie => actions.increaseCountSuccess(movie));
  const failureActions = Object.keys(movies).map(id => actions.increaseCountFailure(error, id));

  it("calls API and dispatches correct actions", () => {
    return expectSaga(
      increaseMultipleSaga,
      actions.increaseCountMultiple(Object.keys(movies), "test")
    )
      .withState({ movies: { byId: movies } })
      .provide({ call: ({ args }) => args[0] })
      .call(actions.doIncreaseCount, movies[1], "test")
      .call(actions.doIncreaseCount, movies[2], "test")
      .put(requestActions)
      .put(successActions)
      .put.like({
        action: {
          title: "Updated 2 movies successfully",
        },
      })
      .run();
  });

  it("handles API errors", () => {
    return expectSaga(
      increaseMultipleSaga,
      actions.increaseCountMultiple(Object.keys(movies), "test")
    )
      .withState({ movies: { byId: movies } })
      .provide({ call: () => { throw error; } })
      .put(failureActions)
      .put.like({
        action: {
          title: "test error",
        },
      })
      .run();
  });
});

describe("when deleting multiple movies", () => {
  const ids = [1, 2];
  const error = new Error("test error");
  const requestActions = ids.map(id => actions.deleteMovieRequest(id));
  const successActions = ids.map(id => actions.deleteMovieSuccess(id));
  const failureActions = ids.map(id => actions.deleteMovieFailure(error, id));

  it("calls API and dispatches correct actions", () => {
    return expectSaga(deleteMultipleSaga, actions.deleteMovieMultiple(ids, "test"))
      .provide({
        race: () => [false],
        call: () => false,
        select: () => 1,
      })
      .call(api.deleteMovie, 1, "test")
      .call(api.deleteMovie, 2, "test")
      .put(requestActions)
      .put(successActions)
      .put(updateFilter({ [filterIds]: "1,2" }))
      .put(actions.deleteMoviesCompleted())
      .put(trimFilter(filterIds))
      .call(notifySuccess, "Deleted 2 movies successfully")
      .run();
  });

  it("handles undo", () => {
    return expectSaga(deleteMultipleSaga, actions.deleteMovieMultiple(ids, "test"))
      .provide({
        race: () => [true],
        call: () => false,
        select: () => 1,
      })
      .not.call(api.deleteMovie, 1, "test")
      .not.call(api.deleteMovie, 2, "test")
      .put(resetPages(1))
      .run();
  });

  it("handles API errors", () => {
    return expectSaga(deleteMultipleSaga, actions.deleteMovieMultiple(ids, "test"))
      .provide([
        [matchers.call.fn(api.deleteMovie), throwError(error)],
        {
          call: () => false,
          select: () => 1,
        },
      ])
      .put(failureActions)
      .put.like({
        action: {
          title: "test error",
        },
      })
      .run();
  });
});

describe("when a movie is deleted", () => {
  describe("and the movie deleted was the last movie on the last page", () => {
    it("should switch to the previous page", () =>
      expectSaga(deleteMoviesCompleted)
        .provide([
          [select(getCurrentPage, "movies"), 2],
          [select(getCurrentPageResults), []]
        ])
        .put(resetPages(1))
        .run());
  });

  describe("and one or more movies remain", () => {
    it("should not switch to the previous page", () =>
      expectSaga(deleteMoviesCompleted)
        .provide([
          [select(getCurrentPage, "movies"), 2],
          [select(getCurrentPageResults), [{ id: 1 }]],
        ])
        .put(resetPages(2))
        .run());
  });

  it("should notify the user of success with the ability to undo", () => {
    const generator = notifySuccess("test message");
    const successNotification = generator.next().value;
    expect(successNotification).toMatchObject(
      put({
        title: "test message",
        action: {
          label: "Undo",
          callback: expect.any(Function)
        },
      })
    );
    // Returns a Promise that resolves upon user clicking "Undo"
    const notificationPromise = generator.next().value;
    expect(notificationPromise).toBeInstanceOf(Promise);
    expect(generator.next().done).toEqual(true);
    // Invoke notification callback
    successNotification.payload.action.action.callback();
    return expect(notificationPromise).resolves.toBe(true);
  });
});
