import React from "react";
import { Route } from "react-router";
import { Switch } from "react-router-dom";

import Home from "./Home";
import MoviesContainer from "./containers/MoviesContainer";
import AdminComponent from "./components/Admin";
import LoginContainer from "./containers/LoginContainer";

import { userIsAuthenticatedRedir, userIsNotAuthenticatedRedir, userIsAdminRedir } from "./helpers/auth";

const Login = userIsNotAuthenticatedRedir(LoginContainer);
const Movies = userIsAuthenticatedRedir(MoviesContainer);
const Admin = userIsAuthenticatedRedir(userIsAdminRedir(AdminComponent));

export default (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/movies" component={Movies} />
    <Route path="/login" component={Login} />
    <Route path="/admin" component={Admin} />
  </Switch>
);
