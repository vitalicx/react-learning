import React from "react";
import { shallow } from "enzyme";
import { TemplatedForm } from "./TemplatedForm";
import { Form, Button } from "antd";
import { getOverflowOptions } from "antd/lib/tooltip/placements";

describe("<TemplatedForm />", () => {
  const props = {
    handleSubmit: jest.fn(),
    handleReset: jest.fn(),
    isSubmitting: true,
  };

  describe("by default", () => {
    const wrap = shallow(
      <TemplatedForm {...props}>
        <input />
      </TemplatedForm>
    );

    it("renders a `Form` component", () => {
      expect(wrap.find(Form).exists()).toBeTruthy();
    });

    it("renders a submit and reset `Button`", () => {
      expect(
        wrap.findWhere(n => n.type() === Button && n.props().children === "Submit").exists()
      ).toBeTruthy();
      expect(
        wrap.findWhere(n => n.type() === Button && n.props().children === "Reset").exists()
      ).toBeTruthy();
    });

    it("passes `children` prop to `Form` component", () => {
      expect(wrap.find("input").exists()).toBeTruthy();
    });

    it("calls `handleSubmit` when form is submitted", () => {
      wrap.find(Form).simulate("submit");
      expect(props.handleSubmit).toHaveBeenCalled();
    });

    it("disables the reset button when form is not `dirty`", () => {
      expect(wrap.find(Button).first().prop("disabled")).toBeTruthy();
    });

    it("calls `handleReset` when reset button is clicked", () => {
      wrap.find(Button).first().simulate("click");
      expect(props.handleReset).toHaveBeenCalled();
    });

    it("disables the submit button if `isSubmitting` prop to set true", () => {
      expect(wrap.find(Button).last().prop("disabled")).toBeTruthy();
    });
  });

  describe("when `dirty` is true and `isSubmitting` is false", () => {
    const newProps = { ...props, dirty: true, isSubmitting: false};

    const wrap = shallow(
      <TemplatedForm {...newProps}>
        <input />
      </TemplatedForm>
    );

    it("enables the reset button", () => {
      expect(wrap.find(Button).first().prop("disabled")).toBeFalsy();
    });
  });

  describe("when `dirty` is true and `isSubmitting` is true", () => {
    const newProps = { ...props, dirty: true, isSubmitting: true };

    const wrap = shallow(
      <TemplatedForm {...newProps}>
        <input />
      </TemplatedForm>
    );

    it("disables the reset button", () => {
      expect(wrap.find(Button).first().prop("disabled")).toBeTruthy();
    });
  });
});