import React, { Component } from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { getPagination } from "./../helpers/pagination/selectors";

export const withPaging = BaseComponent => {
  return class PaginatedList extends Component {
    static propTypes = {
      pagination: PropTypes.shape({
        currentPage: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
        pageSize: PropTypes.number.isRequired,
        filter: PropTypes.object.isRequired,
      }),
      requestPage: PropTypes.func.isRequired,
      resetPages: PropTypes.func.isRequired,
      setPageSize: PropTypes.func.isRequired,
      setPageSorting: PropTypes.func.isRequired,
    };

    getPagination = ({ total, currentPage, pageSize }) => ({
      showSizeChanger: true,
      showQuickJumper: true,
      pageSizeOptions: ["5", "10", "25", "50", "100"],
      total: total,
      current: currentPage,
      pageSize: pageSize,
      showTotal: (total, range) => `${range[0]} to ${range[1]} of ${total}`,
    });

    getCurrentPageAfterPageSizeChange = pageSize => {
      const { currentPage, total } = this.props.pagination;
      const newLastPage = Math.ceil(total / pageSize);
      return Math.min(newLastPage, currentPage);
    };

    handlePageChange = page => this.props.requestPage(page);

    handlePageSizeChange = pageSize => {
      this.props.setPageSize(pageSize);
      const currentPage = this.getCurrentPageAfterPageSizeChange(pageSize);
      this.props.resetPages(currentPage);
    };

    handleSortChange = (field, order) => {
      this.props.setPageSorting(field, order);
      this.props.resetPages(this.props.pagination.currentPage);
    };

    handleChange = (pagination, _, sorter) => {
      const { currentPage, pageSize: currentPageSize, filter } = this.props.pagination;
      const { current, pageSize } = pagination;
      if (pageSize !== currentPageSize) {
        this.handlePageSizeChange(pageSize);
      }
      if (filter.sortField !== sorter.field || filter.sortOrder !== sorter.order) {
        this.handleSortChange(sorter.field, sorter.order);
      }
      if (current !== currentPage) {
        this.handlePageChange(current);
      }
    };

    render() {
      const { pagination, ...props } = this.props;
      delete props.requestPage;
      delete props.resetPages;
      delete props.setPageSize;
      delete props.setPageSorting;

      return (
        <BaseComponent
          onChange={this.handleChange}
          pagination={this.getPagination(pagination)}
          filter={pagination.filter}
          {...props}
        />
      );
    }
  };
};

export const mapStateToProps = (state, props) => ({
  pagination: getPagination(state, props.stateKey),
});

export default ({ requestPage, resetPages, setPageSize, setPageSorting }) =>
  compose(
    connect(
      mapStateToProps,
      { requestPage, resetPages, setPageSize, setPageSorting }
    ),
    withPaging
  );
