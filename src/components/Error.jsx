import React from "react";
import PropTypes from "prop-types";
import { Alert } from "antd";

const Error = ({ message, onRetry }) => (
  <Alert
    type="error"
    message="Error occurred fetching movies"
    description={message}
    closable
    showIcon
    closeText="Retry"
    onClose={onRetry}
  />
);

Error.propTypes = {
  message: PropTypes.string.isRequired,
  onRetry: PropTypes.func.isRequired,
};

export default Error;