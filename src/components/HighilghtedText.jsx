import React from "react";
import PropTypes from "prop-types";
import styles from "../styles/scss/components/HighlightedText.module.scss"; // Import css modules stylesheet as styles

const HighlightedText = ({ text, filter }) => {
  if (!filter) {
    return text;
  }
  const fragments = text.split(new RegExp(`(${filter})`, "gi"));
  return fragments.map((fragment, i) =>
    fragment.toLowerCase() === filter.toLowerCase() ? (
      <span key={i} className={styles.highlight}>
        {fragment}
      </span>
    ) : (
      fragment
    )
  );
};

HighlightedText.propTypes = {
  text: PropTypes.string.isRequired,
  filter: PropTypes.string,
};

export default HighlightedText;
