import React from "react";
import { shallow } from "enzyme";
import { Movies } from "./Movies";
import MovieListContainer from "../containers/MovieListContainer";
import MovieFormContainer from "../containers/MovieFormContainer";
import MovieSearchContainer from "../containers/MovieSearchContainer";
import Error from "./Error";

describe("<Movies />", () => {
  const actions = {
    handleAdd: jest.fn(),
    handleRetry: jest.fn(),
    handleSearch: jest.fn(),
  };

  describe("by default", () => {
    const wrap = shallow(<Movies isLoading={false} actions={actions} />);

    it("should render a title", () => {
      expect(wrap.containsMatchingElement(<h1>My Movies</h1>)).toBeTruthy();
    });

    it("should render a list of movies", () => {
      expect(wrap.find(MovieListContainer).exists()).toBeTruthy();
    });

    describe("the rendered `MovieListContainer`", () => {
      it("should have the `isLoading` prop passed to it", () => {
        expect(wrap.find(MovieListContainer).prop("isLoading")).toBe(false);
      });
    });

    it("should render a movies search box", () => {
      expect(wrap.find(MovieSearchContainer).exists()).toBeTruthy();
    });

    describe("the rendered `MovieSearchContainer`", () => {
      it("should receive as its `onSearch` prop the passed `handleSearch` prop", () => {
        expect(wrap.find(MovieSearchContainer).prop("onSearch")).toBe(actions.handleSearch);
      });
    });

    it("should render a movie entry form", () => {
      expect(wrap.find(MovieFormContainer).exists()).toBeTruthy();
    });

    describe("the rendered `MovieFormContainer`", () => {
      it("should receive as its `onSubmit` prop the passed `handleAdd` prop", () => {
        expect(wrap.find(MovieFormContainer).prop("onSubmit")).toBe(actions.handleAdd);
      });
    });
  });

  describe("when an `error` prop object is passed", () => {
    const error = { message: "test" };
    const wrap = shallow(<Movies isLoading={false} error={error} actions={actions} />);

    it("should render an error message", () => {
      expect(wrap.find(Error).prop("message")).toEqual("test");
    });

    describe("the rendered `Error`", () => {
      it("should receive as its `onRetry` prop the passed `handleRetry` prop", () => {
        expect(wrap.find(Error).prop("onRetry")).toBe(actions.handleRetry);
      });
    });
  });

  describe("when no `error` prop is passed, `isLoading` prop set to false", () => {
    const wrap = shallow(<Movies isLoading={false} actions={actions} />);

    it("should not render an error message", () => {
      expect(wrap.find(Error).length).toBe(0);
    });
  });
});
