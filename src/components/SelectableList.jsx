import React, { Component } from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { setSelectedRows } from "../actions/selectedRows";
import { getSelectedRowKeys } from "../reducers/ui/selected/selectedRowKeys";

export const withSelection = BaseComponent => {
  return class SelectableList extends Component {
    static propTypes = {
      selectedRowKeys: PropTypes.array.isRequired,
      stateKey: PropTypes.string.isRequired,
      setSelectedRows: PropTypes.func.isRequired,
    };

    onSelectChange = selectedRowKeys => {
      this.props.setSelectedRows(this.props.stateKey, selectedRowKeys);
    };

    render() {
      const { selectedRowKeys, ...props } = this.props;
      delete props.setSelectedRows;
      const rowSelection = {
        selectedRowKeys: selectedRowKeys.asMutable(),
        onChange: this.onSelectChange,
      };
      return <BaseComponent rowSelection={rowSelection} {...props} />;
    }
  };
};

export const mapStateToProps = (state, props) => ({
  selectedRowKeys: getSelectedRowKeys(state, props.stateKey),
});

export default compose(
  connect(
    mapStateToProps,
    { setSelectedRows }
  ),
  withSelection
);
