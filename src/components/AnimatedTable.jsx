import React from "react";
import Animate from "rc-animate";
import styles from "../styles/scss/components/AnimatedTable.module.scss";

const AnimatedTable = props => (
  <Animate
    component="tbody"
    transitionLeave={false}
    transitionName={{
      enter: styles.enter,
      enterActive: styles.enterActive,
    }}
    showProp="showHeader"
    {...props}
  />
);

export default AnimatedTable;