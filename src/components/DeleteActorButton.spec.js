import React from "react";
import { shallow } from "enzyme";
import DeleteActorButton from "./DeleteActorButton";
import { Button } from "antd";

describe("<DeleteActorButton />", () => {
  const mockDelete = jest.fn();
  const wrap = shallow(<DeleteActorButton onDelete={mockDelete} loading={false} />);

  it("calls `onDelete` callback prop after a confirmed delete", async () => {
    await wrap.prop("onConfirm")();
    expect(mockDelete).toHaveBeenCalled();
  });

  it("renders a `Button` and passes through `loading` prop", () => {
    expect(wrap.find(Button).prop("loading")).toBeFalsy();
  });
});
