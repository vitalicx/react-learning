import React from "react";
import { shallow } from "enzyme";
import MovieForm from "./MovieForm";
import TextInput from "./inputs/TextInput";
import SelectInput from "./inputs/SelectInput";

describe("<MovieForm />", () => {
  const handleSearch = jest.fn();
  const actorsData = [];
  const wrap = shallow(
    <MovieForm actors={actorsData} isLoading={false} onSearch={handleSearch} onSubmit={jest.fn()} />
  );
  const innerForm = () =>
    wrap
      .dive()
      .children() // TemplatedFormOuter
      .dive(); // TemplatedForm

  it("renders the `title` field", () => {
    expect(
      innerForm()
        .find({ name: "title" })
        .prop("component")
    ).toBe(TextInput);
  });

  it("renders the `actors` field", () => {
    expect(
      innerForm()
        .find({ name: "actors" })
        .prop("component")
    ).toBe(SelectInput);
  });

  it("passes correct default values", () => {
    expect(innerForm().prop("values")).toMatchObject({ title: "", actors: [] });
  });

  it("prevents empty form submission", async () => {
    const errors = await innerForm()
      .props()
      .validateForm();
    expect(errors).toMatchObject({
      title: "Movie title is required.",
      actors: "At least one actor is required",
    });
  });

  describe("the rendered `actors` field", () => {
    const actors = innerForm().find({ name: "actors" });

    it("receives `handleSearch` callback as `onSearch` prop", () => {
      actors.props().onSearch();
      expect(handleSearch).toHaveBeenCalled();
    });

    it("receives parent `actors` prop as `dataSource` prop", () => {
      expect(actors.prop("dataSource")).toBe(actorsData);
    });

    it("receives parent `isLoading` prop", () => {
      expect(actors.prop("isLoading")).toBe(false);
    });
  });
});
