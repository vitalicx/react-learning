import React from "react";
import { shallow } from "enzyme";
import DeleteButton from "./DeleteButton";

describe("<DeleteButton />", () => {
  const mockDelete = jest.fn();
  const wrap = shallow(<DeleteButton onDelete={mockDelete} loading={false} size="large" />);

  it("calls `onDelete` callback prop after a confirmed delete", async () => {
    await wrap.prop("onConfirm")();
    expect(mockDelete).toHaveBeenCalled();
  });

  it("passes additional props to `Button`", () => {
    expect(wrap.children().prop("size")).toBe("large");
  });
});
