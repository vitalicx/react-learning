import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { Card, Table } from "antd";
import HighlightedText from "./HighilghtedText";
import { moviesPaginator } from "../paginators";
import withPaging from "./PaginatedList";
import withSelection from "./SelectableList";
import MovieListActorsContainer from "../containers/MovieListActorsContainer";
import MovieListRowActions from "./MovieListRowActions";
import MovieListActions from "./MovieListActions";
import AnimatedTable from "./AnimatedTable";

const Column = Table.Column;

const expandedRowRender = movie => <MovieListActorsContainer movie={movie} />;

const actionsRender = (actions, selectedRowKeys, onIncrease, onDelete) => (
  <MovieListActions
    actions={actions}
    selectedRowKeys={selectedRowKeys}
    onIncrease={onIncrease}
    onDelete={onDelete}
  />
);

export const MovieList = ({
  movies,
  pagination,
  isLoading,
  filter: { title: filterText },
  actions,
  onIncrease,
  onDelete,
  onChange,
  rowSelection,
}) => (
  <Card
    type="inner"
    bodyStyle={{ padding: 0 }}
    title={actionsRender(actions, rowSelection.selectedRowKeys, onIncrease, onDelete)}
  >
    <Table
      dataSource={movies}
      rowKey="id"
      rowSelection={rowSelection}
      pagination={pagination}
      expandedRowRender={expandedRowRender}
      onChange={onChange}
      components={{ body: { wrapper: AnimatedTable } }}
      loading={isLoading}
    >
      <Column title="ID" dataIndex="id" key="id" />
      <Column
        title="Title"
        dataIndex="title"
        key="title"
        sorter={true}
        render={text => (isLoading ? text : <HighlightedText text={text} filter={filterText} />)}
      />
      <Column title="Bad Puns Count" dataIndex="trackCount" key="trackCount" sorter={true} />
      <Column title="No. Actors" dataIndex="numActors" key="numActors" />
      <Column
        title="Actions"
        key="actions"
        render={(text, movie) => (
          <MovieListRowActions
            movie={movie}
            actions={actions}
            onIncrease={onIncrease}
            onDelete={onDelete}
          />
        )}
      />
      {!!filterText && <Column title="Rank" dataIndex="rank" key="rank" sorter={true} />}
    </Table>
  </Card>
);

MovieList.propTypes = {
  movies: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  onIncrease: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  rowSelection: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  filter: PropTypes.shape({
    title: PropTypes.string,
  }).isRequired,
};

export const enhance = compose(
  withSelection,
  withPaging(moviesPaginator)
);

export default enhance(MovieList);
