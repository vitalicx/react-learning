import React from "react";
import PropTypes from "prop-types";
import { Divider } from "antd";
import IncreaseCountButton from "../components/IncreaseCountButton";
import DeleteButton from "../components/DeleteButton";
import { getIsDeleting, getIsIncreasing } from "../reducers/movies/actions";

const MovieListRowActions = ({ actions, movie, onIncrease, onDelete }) => (
  <div>
    <IncreaseCountButton
      onIncrease={() => onIncrease(movie)}
      loading={getIsIncreasing(actions, movie.id)}
    />
    <Divider type="vertical" />
    <DeleteButton
      onDelete={() => onDelete(movie)}
      loading={getIsDeleting(actions, movie.id)}
    />
  </div>
);

MovieListRowActions.propTypes = {
  actions: PropTypes.object.isRequired,
  movie: PropTypes.object.isRequired,
  onIncrease: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default MovieListRowActions;
