import React from "react";
import PropTypes from "prop-types";
import Error from "./Error";
import MovieListContainer from "../containers/MovieListContainer";
import MovieSearchContainer from "../containers/MovieSearchContainer";
import MovieFormContainer from "../containers/MovieFormContainer";

export const Movies = props => {
  const { isLoading, error, actions } = props;
  const { handleAdd, handleRetry, handleSearch } = actions;
  return (
    <>
      <h1>My Movies</h1>
      {error.message && <Error message={error.message} onRetry={handleRetry} />}
      <MovieSearchContainer onSearch={handleSearch} />
      <MovieListContainer isLoading={isLoading} />
      <MovieFormContainer onSubmit={handleAdd} />
    </>
  );
};

Movies.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.object,
  actions: PropTypes.object.isRequired,
};

Movies.defaultProps = {
  error: {}
};