import React from "react";
import PropTypes from "prop-types";
import * as Yup from "yup";
import createForm from "../helpers/form";
import { Field, FastField } from "formik";
import TextInput from "./inputs/TextInput";
import SelectInput from "./inputs/SelectInput";
import styles from "../styles/scss/components/ActorForm.module.scss";

const formConfig = {
  displayName: "ActorForm",
  validationSchema: Yup.object().shape({
    name: Yup.string()
      .min(2, "C'mon, your name is longer than that")
      .required("Actor name is required."),
    movies: Yup.string().required("At least one movie is required"),
  }),
  mapPropsToValues: props => ({ name: props.name || "", movies: props.movieSelectDefault }),
};

const ActorForm = createForm(formConfig)(props => (
  <>
    <FastField
      name="name"
      required
      autoComplete="off"
      placeholder="enter actor name"
      component={TextInput}
      className={styles.name}
    />
    <Field
      name="movies"
      required
      autoComplete="off"
      placeholder="Select movies"
      component={SelectInput}
      onSearch={props.onSearch}
      dataSource={props.movieSelectData}
      isLoading={props.isLoading}
    />
  </>
));

ActorForm.propTypes = {
  movieSelectDefault: PropTypes.array.isRequired,
  movieSelectData: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onSearch: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default ActorForm;
