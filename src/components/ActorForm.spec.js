import React from "react";
import { shallow } from "enzyme";
import ActorForm from "./ActorForm";
import TextInput from "./inputs/TextInput";
import SelectInput from "./inputs/SelectInput";

describe("<ActorForm />", () => {
  const props = {
    movieSelectData: [],
    movieSelectDefault: [],
    isLoading: false,
    onSearch: jest.fn(),
    onSubmit: jest.fn(),
  };

  const getInnerForm = wrap =>
    wrap
      .dive()
      .children() // TemplatedFormOuter
      .dive(); // TemplatedForm

  describe("by default", () => {
    const wrap = shallow(<ActorForm {...props} />);
    const innerForm = getInnerForm(wrap);

    it("renders the `name` field", () => {
      expect(innerForm.find({ name: "name" }).prop("component")).toBe(TextInput);
    });

    it("renders the `movies` field", () => {
      expect(innerForm.find({ name: "movies" }).prop("component")).toBe(SelectInput);
    });

    it("passes correct default values", () => {
      expect(innerForm.prop("values")).toMatchObject({ name: "", movies: [] });
    });

    it("prevents empty form submission", async () => {
      const errors = await innerForm.props().validateForm();
      expect(errors).toMatchObject({
        name: "Actor name is required.",
        movies: "At least one movie is required",
      });
    });

    describe("the rendered `movies` field", () => {
      const movies = innerForm.find({ name: "movies" });

      it("receives `handleSearch` callback as `onSearch` prop", () => {
        movies.props().onSearch();
        expect(props.onSearch).toHaveBeenCalled();
      });

      it("receives parent `movies` prop as `dataSource` prop", () => {
        expect(movies.prop("dataSource")).toBe(props.movieSelectData);
      });

      it("receives parent `isLoading` prop", () => {
        expect(movies.prop("isLoading")).toBe(false);
      });
    });
  });

  describe("when a movie is selected", () => {
    const propsWithDefaultMovie = {
      ...props,
      movieSelectDefault: [0],
    };
    const wrap = shallow(<ActorForm {...propsWithDefaultMovie} />);
    const innerForm = getInnerForm(wrap);

    it("sets `movieSelectDefault` prop to default value for `movies` field", () => {
      expect(innerForm.prop("values")).toMatchObject({ movies: [0] });
    });
  });
});
