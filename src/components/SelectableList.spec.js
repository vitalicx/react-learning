import React from "react";
import { shallow } from "enzyme";
import Immutable from "seamless-immutable";
import { withSelection, mapStateToProps } from "./SelectableList";

describe("withSelection()", () => {
  const wrapped = () => <div />;
  const SelectableList = withSelection(wrapped);
  const selectedMovies = Immutable([1]);
  const setSelectedRowsSpy = jest.fn();

  const wrap = shallow(
    <SelectableList
      stateKey="movies"
      additionalProp="test"
      selectedRowKeys={selectedMovies}
      setSelectedRows={setSelectedRowsSpy}
    />
  );

  it("renders the wrapped component", () => {
    expect(wrap.is(wrapped)).toBeTruthy();
  });

  it("passes additional props through to wrapped component", () => {
    expect(wrap.prop("additionalProp")).toMatch("test");
  });

  it("removes action creators from props pass-through", () => {
    expect(wrap.props()).not.toHaveProperty("setSelectedRows");
  });

  describe("the `rowSelection` prop passed to wrapped component", () => {
    const rowSelection = wrap.prop("rowSelection");
    it("should contain `selectedRowKeys` array", () => {
      expect(rowSelection.selectedRowKeys).toEqual(selectedMovies);
    });

    it("should contain an `onChange` callback that dispatches `setSelectedRows`", () => {
      rowSelection.onChange([1, 2]);
      expect(setSelectedRowsSpy).toHaveBeenCalledWith("movies", [1, 2]);
    });
  });

  it("should pass the correct props from state", () => {
    const state = { ui: { selectedRowKeys: { movies: selectedMovies } } };
    const props = { stateKey: "movies" };
    expect(mapStateToProps(state, props)).toMatchObject({
      selectedRowKeys: selectedMovies,
    });
  });
});
