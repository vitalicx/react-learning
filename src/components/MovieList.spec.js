import React from "react";
import { shallow } from "enzyme";
import { MovieList, enhance } from "./MovieList";
import { Card, Table } from "antd";
import HighlightedText from "./HighilghtedText";
import { MovieListActorsContainer } from "./../containers/MovieListActorsContainer";
import MovieListActions from "./MovieListActions";
import MovieListRowActions from "./MovieListRowActions";
import withPaging from "./PaginatedList";
import withSelection from "./SelectableList";
import { moviesPaginator } from "../paginators";

jest.mock("./PaginatedList", () => jest.fn(() => () => null));
jest.mock("./SelectableList", () => jest.fn(() => () => null));

const Column = Table.Column;

describe("<MovieList />", () => {
  const movies = [{ title: "test" }];
  const onDeleteeSpy = jest.fn();
  const onIncreaseSpy = jest.fn();
  const onChangeSpy = jest.fn();
  const defaultProps = {
    filter: {},
    pagination: {},
    isLoading: false,
  };
  const actions = {};
  const rowSelection = { selectedRowKeys: [] };
  const render = (props = defaultProps) =>
    shallow(
      <MovieList
        movies={movies}
        actions={actions}
        rowSelection={rowSelection}
        onIncrease={onIncreaseSpy}
        onDelete={onDeleteeSpy}
        onChange={onChangeSpy}
        {...props}
      />
    );

  describe("by default", () => {
    const wrap = render();
    const table = wrap.children();

    it("renders a `Card`", () => {
      expect(wrap.is(Card)).toBeTruthy();
    });

    it("`Card` title prop returns `MovieListActions`", () => {
      expect(wrap.wrap(wrap.prop("title")).type()).toBe(MovieListActions);
    });

    it("passes a `Table` to `Card`", () => {
      expect(table.is(Table)).toBeTruthy();
    });

    it("passes `movies` prop to `Table` as `dataSource`", () => {
      expect(table.prop("dataSource")).toBe(movies);
    });

    it("passes `rowSelection` prop to `Table`", () => {
      expect(table.prop("rowSelection")).toBe(rowSelection);
    });

    it("passes `pagination` prop to `Table`", () => {
      expect(table.prop("pagination")).toBe(defaultProps.pagination);
    });

    it("passes `isLoading` prop to `Table` `loading` prop", () => {
      expect(table.prop("loading")).toBe(defaultProps.isLoading);
    });

    it("passes `onChange` prop to `Table`", () => {
      table.props().onChange("test");
      expect(onChangeSpy).toHaveBeenCalledWith("test");
    });

    it("renders a `MovieListActorsContainer` as the `Table` expanded row", () => {
      const expandedRowRender = table.prop("expandedRowRender");
      // WrappedComponent contains component wrapped by Redux connect()
      expect(table.wrap(expandedRowRender({})).type().WrappedComponent).toBe(
        MovieListActorsContainer
      );
    });

    it("renders correct `Column`s", () => {
      const expectedColumns = {
        id: "ID",
        title: "Title",
        trackCount: "Bad Puns Count",
        numActors: "No. Actors",
        action: "Actions",
      };
      const columns = table.children();
      for (const key in expectedColumns) {
        expect(
          columns.containsMatchingElement(<Column key={key} title={expectedColumns[key]} />)
        ).toBeTruthy();
      }
      expect(columns.containsMatchingElement(<Column title="Rank" />)).toBeFalsy();
    });

    it("Actions `Column` render prop returns `MovieListRowActions`", () => {
      const actionsColumnRender = table.find({ title: "Actions" }).prop("render");
      expect(wrap.wrap(actionsColumnRender("", {})).type()).toBe(MovieListRowActions);
    });
  });

  describe("when a search filter is provided", () => {
    const wrap = render({...defaultProps, filter: { title: "test" }});

    it("renders rank column", () => {
      expect(wrap.children().containsMatchingElement(<Column title="Rank" />)).toBeTruthy();
    });

    it("passes `filter` prop to title `Column` `HighlightedText` component", () => {
      const column = wrap.children().find({ title: "Title" });
      const renderedColumn = wrap.wrap(column.props().render("test"));
      expect(renderedColumn.type()).toBe(HighlightedText);
      expect(renderedColumn.prop("text")).toBe("test");
      expect(renderedColumn.prop("filter")).toBe("test");
    });
  });

  describe("when passed pagination prop `loading` property is `true`", () => {
    const wrap = render({ ...defaultProps, isLoading: true });

    it("does not render highlighted text", () => {
      const column = wrap.children().find({ title: "Title" });
      const renderedColumn = wrap.wrap(column.props().render("test"));
      expect(renderedColumn.text()).toBe("test");
    });
  });

  describe("when enhanced", () => {
    enhance(<div />);
    it("handles selection", () => {
      expect(withSelection).toHaveBeenCalled();
    });
    it("handles pagination", () => {
      expect(withPaging).toHaveBeenCalledWith(moviesPaginator);
    });
  });
});
