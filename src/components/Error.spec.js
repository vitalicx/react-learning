import React from "react";
import { shallow } from "enzyme";
import Error from "./Error";

describe("<Error />", () => {
  const mockRetry = jest.fn();
  const wrap = shallow(<Error message="test" onRetry={mockRetry} />);

  it("renders correctly", () => {
    const alert = wrap.dive();
    expect(alert.find("a").text()).toEqual("Retry");
    expect(alert.find(".ant-alert-message").text()).toEqual("Error occurred fetching movies");
    expect(alert.find(".ant-alert-description").text()).toEqual("test");
  });

  it("handles closing", () => {
    wrap.prop("onClose")();
    expect(mockRetry).toHaveBeenCalled();
  });
});
