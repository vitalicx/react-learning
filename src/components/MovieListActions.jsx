import React from "react";
import PropTypes from "prop-types";
import { Divider } from "antd";
import Animate from "rc-animate";
import IncreaseCountButton from "./IncreaseCountButton";
import DeleteButton from "./DeleteButton";
import styles from "../styles/scss/components/MovieListActions.module.scss";
import { getActionsPending } from "../reducers/movies/actions";

const MovieListActions = ({ actions, selectedRowKeys, onIncrease, onDelete }) => {
  const loading = getActionsPending(actions);
  return (
    <Animate
      transitionName={{
        leaveActive: styles.leaveActive,
        enter: styles.enter,
        enterActive: styles.enterActive,
      }}
      transitionAppear
      transitionleave
    >
      {selectedRowKeys.length ? (
        <div className={styles.panel}>
          <span>
            {selectedRowKeys.length} {selectedRowKeys.length > 1 ? "items" : "item"} selected
          </span>
          <Divider type="vertical" />
          <IncreaseCountButton
            size="large"
            type="normal"
            onIncrease={() => onIncrease(selectedRowKeys)}
            loading={loading}
            disabled={loading}
          />
          <Divider type="vertical" />
          <DeleteButton
            size="large"
            type="danger"
            onDelete={() => onDelete(selectedRowKeys)}
            loading={loading}
            disabled={loading}
          />
        </div>
      ) : null}
    </Animate>
  );
};

MovieListActions.propTypes = {
  actions: PropTypes.object.isRequired,
  selectedRowKeys: PropTypes.array.isRequired,
  onIncrease: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default MovieListActions;
