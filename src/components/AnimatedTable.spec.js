import React from "react";
import { shallow } from "enzyme";
import AnimatedTable from "./AnimatedTable";
import Animate from "rc-animate";

describe("<AnimatedTable />", () => {
  const wrap = shallow(<AnimatedTable additionalProp="test" />);

  it("renders a single `Animate`", () => {
    expect(wrap.is(Animate)).toBeTruthy();
  });

  it("passes the correct styles to `Animate`", () => {
    expect(wrap.prop("transitionName")).toMatchObject({
      enter: "enter",
      enterActive: "enterActive",
    });
  });

  it("passes through props to `Animate`", () => {
    expect(wrap.find(Animate).prop("additionalProp")).toEqual("test");
  });
});
