import React from "react";
import { shallow } from "enzyme";
import LoginForm from "./LoginForm";

describe("<LoginForm />", () => {
  const wrap = shallow(<LoginForm />);
  const innerForm = () =>
    wrap
      .dive()
      .children() // TemplatedFormOuter
      .dive(); // TemplatedForm

  it("renders correct fields", () => {
    const form = innerForm();
    expect(form.find({ name: "email" }).exists()).toBeTruthy();
    expect(form.find({ name: "password" }).exists()).toBeTruthy();
  });

  it("passes correct default values", () => {
    expect(innerForm().prop("values")).toEqual(
      expect.objectContaining({
        email: "",
        password: "",
      })
    );
  });

  it("prevents empty form submission", async () => {
    const errors = await innerForm().props().validateForm();
    expect(errors).toMatchObject({
      email: "Email or username is required.",
      password: "Password is required.",
    });
  });
});
