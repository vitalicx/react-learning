import React from "react";
import { shallow } from "enzyme";
import IncreaseCountButton from "./IncreaseCountButton";
import { Button } from "antd";

describe("<IncreaseCountButton />", () => {
  const mockIncrease = jest.fn();
  const wrap = shallow(
    <IncreaseCountButton onIncrease={mockIncrease} loading={false} size="large" />
  );

  it("calls `onIncrease` callback prop when `Button` is clicked", async () => {
    const button = wrap.find(Button);
    await button.simulate("click");
    expect(mockIncrease).toHaveBeenCalled();
  });

  it("passes additional props to `Button`", () => {
    expect(wrap.prop("size")).toBe("large");
  });
});
