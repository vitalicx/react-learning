import React from "react";
import PropTypes from "prop-types";
import { Menu } from "antd";

const MenuItem = Menu.Item;

export const ConditionalMenuItem = ({ condition, children, ...props }) =>
  condition && <MenuItem {...props}>{children}</MenuItem>;

ConditionalMenuItem.propTypes = {
  condition: PropTypes.bool.isRequired,
};
