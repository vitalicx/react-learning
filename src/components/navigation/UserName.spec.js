import React from "react";
import { shallow } from "enzyme";
import { UserName } from "./UserName";

describe("<Username />", () => {
  it("renders username if logged in", () => {
    const user = { data: { name: "test" } };
    const wrap = shallow(<UserName user={user} />);
    expect(wrap.text()).toBe(`Welcome ${user.data.name}`);
  });

  it("renders message if not logged in", () => {
    const user = { data: null };
    const wrap = shallow(<UserName user={user} />);
    expect(wrap.text()).toBe("Not logged in");
  });
});
