import React from "react";
import { shallow } from "enzyme";
import { ConditionalMenuItem } from "./ConditionalMenuItem";
import { Menu } from "antd";

const MenuItem = Menu.Item;

describe("<ConditionalMenuItem />", () => {
  it("renders `MenuItem` if condition is true", () => {
    const wrap = shallow(<ConditionalMenuItem condition={true} />);
    expect(wrap.find(MenuItem).exists()).toBeTruthy();
  });

  it("passes additional props to `MenuItem`", () => {
    const wrap = shallow(<ConditionalMenuItem condition={true} test={true} />);
    expect(wrap.find(MenuItem).prop("test")).toBeTruthy();
  });

  it("passes provided children to `MenuItem`", () => {
    const wrap = shallow(
      <ConditionalMenuItem condition={true}>
        <div />
      </ConditionalMenuItem>
    );
    expect(wrap.children().length).toBe(1);
  });

  it("renders nothing if condition is false", () => {
    const wrap = shallow(<ConditionalMenuItem condition={false} />);
    expect(wrap.isEmptyRender()).toBeTruthy();
  });
});
