import React from "react";
import { shallow } from "enzyme";
import { Navigation } from "./Navigation";
import { Layout, Menu } from "antd";

const { Header } = Layout;

describe("<Navigation />", () => {
  const user = { data: { roles: [] } };

  describe("while logged in, the rendered `Navigation`", () => {
    const wrap = shallow(<Navigation user={user} onLogout={jest.fn()} location={{}} />);

    it("contains a single `Header`", () => {
      expect(wrap.type()).toEqual(Header);
    });

    it("contains a `Menu`", () => {
      expect(wrap.find(Menu).exists()).toBeTruthy();
    });

    it("contains a link to the home page", () => {
      expect(wrap.find({ to: "/" }).exists()).toBeTruthy();
    });

    it("contains a link to log out, but not log in", () => {
      expect(wrap.find({ to: "logout" }).parent().prop("condition")).toBeTruthy();
      expect(wrap.find({ to: "login" }).parent().prop("condition")).toBeFalsy();
    });

    it("doesn't contain an admin link without `ROLE_ADMIN` role", () => {
      expect(wrap.find({ to: "admin" }).parent().prop("condition")).toBeFalsy();
    });
  });

  describe("while logged out, the rendered `Navigation`", () => {
    const wrap = shallow(<Navigation user={{ data: null }} onLogout={jest.fn()} location={{}} />);

    it("contains a link to log in, but not log out", () => {
      expect(wrap.find({ to: "login" }).parent().prop("condition")).toBeTruthy();
      expect(wrap.find({ to: "logout" }).parent().prop("condition")).toBeFalsy();
    });
  });

  describe("while Administrator, the rendered `Navigation`", () => {
    const adminUser = { data: { roles: ["ROLE_ADMIN"] } };
    const wrap = shallow(<Navigation user={adminUser} onLogout={jest.fn()} location={{}} />);

    it("contains a link to admin section", () => {
      expect(wrap.find({ to: "admin" }).parent().prop("condition")).toBeTruthy();
    });
  });

  describe("when `location` prop is passed with a valid `pathname` key", () => {
    const wrap = shallow(<Navigation user={user} onLogout={jest.fn()} location={{ pathname: "/movies" }} />);

    it("highlights the correct menu item", () => {
      expect(wrap.find(Menu).prop("selectedKeys")[0]).toBe("/movies");
    });
  });

  describe("when clicking logout link", () => {
    const onLogoutSpy = jest.fn();
    const wrap = shallow(<Navigation user={user} onLogout={onLogoutSpy} location={{}} />);

    it("calls the onLogout prop", () => {
      wrap.find({ to: "logout" }).simulate("click");
      expect(onLogoutSpy).toBeCalled();
    });
  });
});
