import React from "react";
import PropTypes from "prop-types";
import { getUserData } from "../../reducers/user";

export const UserName = props => {
  const data = getUserData(props);
  return <div className="navbar-text">
    {data ? `Welcome ${data.name}` : "Not logged in"}
  </div>;
};

UserName.propTypes = {
  user: PropTypes.object.isRequired,
};
