import React from "react";
import PropTypes from "prop-types";
import { Layout, Menu } from "antd";
import { Link, withRouter } from "react-router-dom";
import { UserName } from "./UserName";
import { getIsAuthenticated, getIsAdmin } from "../../reducers/user";
import { ConditionalMenuItem } from "./ConditionalMenuItem";

const { Header } = Layout;
const MenuItem = Menu.Item;

export const Navigation = ({ user, onLogout, location }) => {
  const isAuthenticated = getIsAuthenticated({ user });
  const isAdmin = isAuthenticated && getIsAdmin({ user });
  return (
    <Header>
      <UserName user={user} />
      <Menu mode="horizontal" theme="dark" selectedKeys={[location.pathname]} style={{ lineHeight: "64px" }}>
        <MenuItem key="/">
          <Link to="/">React Sample Project</Link>
        </MenuItem>
        <MenuItem key="/movies">
          <Link to="movies">Movies</Link>
        </MenuItem>
        <ConditionalMenuItem key="/admin" condition={isAdmin}>
          <Link to="admin">Admin</Link>
        </ConditionalMenuItem>
        <ConditionalMenuItem key="/login" condition={!isAuthenticated}>
          <Link to="login">Login</Link>
        </ConditionalMenuItem>
        <ConditionalMenuItem condition={isAuthenticated}>
          <Link to="logout" onClick={onLogout}>Logout</Link>
        </ConditionalMenuItem>
      </Menu>
    </Header>
  );
};

Navigation.propTypes = {
  user: PropTypes.object.isRequired,
  onLogout: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired
};

export default withRouter(Navigation);
