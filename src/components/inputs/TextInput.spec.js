import React from "react";
import { shallow } from "enzyme";
import TextInput from "./TextInput";
import { Input } from "antd";
import FormItem from "./FormItem";

describe("<TextInput />", () => {
  const props = {
    field: {
      name: "test",
      type: "email",
    },
    form: {
      errors: {
        test: "test error",
      },
      touched: {
        test: true,
      },
    },
    required: true,
  };

  describe("by default", () => {
    const wrap = shallow(<TextInput {...props} />);

    it("renders an `Input` inside a `FormItem`", () => {
      expect(wrap.type()).toBe(FormItem);
      expect(wrap.children().type()).toBe(Input);
    });

    it("passes any additional props to `Input` component", () => {
      expect(wrap.find(Input).prop("type")).toBe("email");
      expect(wrap.find(Input).prop("required")).toBeTruthy();
    });
  });
});
