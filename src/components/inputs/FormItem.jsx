import React from "react";
import PropTypes from "prop-types";
import { Form } from "antd";

const FormItem = ({ form: { touched, errors }, name, children }) => (
  <Form.Item
    hasFeedback={touched[name] && !!errors[name]}
    validateStatus={touched[name] && errors[name] && "error"}
    help={touched[name] && errors[name]}
  >
    {children}
  </Form.Item>
);

FormItem.propTypes = {
  form: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
};

export default FormItem;