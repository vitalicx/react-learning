import React from "react";
import { Input } from "antd";
import FormItem from "./FormItem";

const TextInput = ({ field: { ...fields }, form, ...props }) => (
  <FormItem form={form} name={fields.name}>
    <Input {...props} {...fields} />
  </FormItem>
);
export default TextInput;
