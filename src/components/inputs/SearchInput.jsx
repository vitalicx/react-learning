import React from "react";
import PropTypes from "prop-types";
import { Input, Icon } from "antd";
import FormItem from "./FormItem";

const Search = Input.Search;

const suffix = (text, onClear) =>
  text ? (
    <Icon key="clearSearch" type="close-circle" onClick={onClear} style={{ marginRight: "10px" }} />
  ) : null;

const SearchInput = ({ form, field, isLoading, onClear, onSearch, setInput, ...props }) => {
  return (
    <FormItem form={form} name={field.name}>
      <Search
        {...props}
        {...field}
        onSearch={onSearch}
        suffix={suffix(form.values[field.name], onClear)}
        placeholder="input search text"
        enterButton="Search"
        size="large"
        disabled={isLoading}
        ref={setInput}
      />
    </FormItem>
  );
};

SearchInput.propTypes = {
  onSearch: PropTypes.func.isRequired,
  onClear: PropTypes.func.isRequired,
  setInput: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  form: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
};

export default SearchInput;
