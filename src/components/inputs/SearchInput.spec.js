import React from "react";
import { shallow, mount } from "enzyme";
import SearchInput from "./SearchInput";
import { Input, Icon } from "antd";
import FormItem from "./FormItem";

const Search = Input.Search;

describe("<SearchInput />", () => {
  const props = {
    field: {
      name: "test",
      size: "large",
    },
    form: {
      errors: {
        test: "test error",
      },
      touched: {
        test: true,
      },
      values: {
        test: "test",
      },
    },
    required: true,
    onSearch: jest.fn(),
    onClear: jest.fn(),
    setInput: jest.fn(),
    isLoading: false,
  };

  describe("by default", () => {
    const wrap = shallow(<SearchInput {...props} />);
    const search = wrap.find(Search);

    it("renders a `Search` inside a `FormItem`", () => {
      expect(wrap.type()).toBe(FormItem);
      expect(wrap.children().type()).toBe(Search);
    });

    it("passes any additional props to `Search`", () => {
      expect(search.prop("size")).toMatch("large");
      expect(search.prop("required")).toBeTruthy();
    });

    it("passes `onSearch` callback prop to `Search`", () => {
      search.props().onSearch();
      expect(props.onSearch).toHaveBeenCalled();
    });

    it("sets `Search` `disabled` prop to passed `isLoading` prop", () => {
      expect(search.prop("disabled")).toBe(false);
    });

    describe("the rendered `suffix` prop passed to `Search`", () => {
      const suffix = wrap.wrap(search.prop("suffix"));
      it("contains a single `Icon`", () => {
        expect(suffix.is(Icon)).toBeTruthy();
      });

      it("sets `Icon` `onClick` prop to passed `onClear` callback prop", () => {
        suffix.props().onClick();
        expect(props.onClear).toHaveBeenCalled();
      });
    });
  });

  describe("when the user has not typed any text", () => {
    const withoutTextProps = { ...props, form: { ...props.form, values: {} } };
    const wrap = shallow(<SearchInput {...withoutTextProps} />);
    const search = wrap.find(Search);

    describe("the rendered `suffix` prop passed to `Search`", () => {
      const suffix = wrap.wrap(search.prop("suffix"));
      test("has no icon visible", () => {
        expect(suffix.type()).toBe(null);
      });
    });
  });

  describe("when mounted", () => {
    mount(<SearchInput {...props} />);
    test("the `setInput` ref callback prop is called", () => {
      expect(props.setInput).toHaveBeenCalled();
    });
  });
});
