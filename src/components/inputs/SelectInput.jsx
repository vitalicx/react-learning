import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Select, Spin } from "antd";
import FormItem from "./FormItem";

const Option = Select.Option;

class SelectInput extends PureComponent {
  handleChange = value => {
    this.props.form.setFieldValue(this.props.field.name, value);
  };

  handleBlur = value => {
    const initialValue = this.props.form.initialValues[this.props.field.name];
    if (initialValue !== value) {
      this.props.form.setFieldTouched(this.props.field.name, true);
    }
  };

  render() {
    const {
      dataSource,
      form,
      field: { ...fields },
      ...props
    } = this.props;

    return (
      <FormItem form={form} name={fields.name}>
        <Select
          {...props}
          {...fields}
          mode="multiple"
          notFoundContent={props.isLoading ? <Spin size="small" /> : null}
          filterOption={false}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
        >
          {dataSource.map(a => (
            <Option key={a.id}>{a.name || a.title}</Option>
          ))}
        </Select>
      </FormItem>
    );
  }
}

SelectInput.propTypes = {
  dataSource: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default SelectInput;
