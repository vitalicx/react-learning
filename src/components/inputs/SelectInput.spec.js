import React from "react";
import { shallow } from "enzyme";
import SelectInput from "./SelectInput";
import { Select, Spin } from "antd";
import FormItem from "./FormItem";

const Option = Select.Option;

describe("<SelectInput />", () => {
  const props = {
    field: {
      name: "test",
      type: "email",
    },
    form: {
      errors: {
        test: "test error",
      },
      touched: {
        test: true,
      },
      setFieldTouched: jest.fn(),
      setFieldValue: jest.fn(),
      initialValues: {
        test: "initial value",
      },
    },
    required: true,
  };

  const actors = [
    {
      id: "1",
      name: "test",
    },
    {
      id: "2",
      title: "test two",
    },
  ];

  describe("by default", () => {
    const wrap = shallow(<SelectInput {...props} isLoading={false} dataSource={actors} />);

    it("renders a `Select` inside a `FormItem`", () => {
      expect(wrap.type()).toBe(FormItem);
      expect(wrap.children().type()).toBe(Select);
    });

    it("passes any additional props to `Select` component", () => {
      expect(wrap.find(Select).prop("type")).toBe("email");
      expect(wrap.find(Select).prop("required")).toBeTruthy();
    });

    it("passes a list of children `Option`s to the `Select` component", () => {
      const selectChildren = wrap.find(Select).children();
      expect(
        selectChildren.containsAllMatchingElements([
          <Option key={1}>test</Option>,
          <Option key={2}>test two</Option>,
        ])
      ).toBeTruthy();
    });

    it("updates the field value on change", () => {
      wrap
        .find(Select)
        .props()
        .onChange("test value");
      expect(props.form.setFieldValue).toHaveBeenCalledWith("test", "test value");
    });
  });

  describe("when `isLoading` prop is set to `true`", () => {
    const wrap = shallow(<SelectInput {...props} isLoading={true} dataSource={[]} />);
    const notFoundContent = wrap.wrap(wrap.find(Select).prop("notFoundContent"));

    it("passes a `Spin` loader to the `notFoundContent` prop of the `Select`", () => {
      expect(notFoundContent.type()).toBe(Spin);
    });
  });

  describe("when `isLoading` prop is set to `false`", () => {
    const wrap = shallow(<SelectInput {...props} isLoading={false} dataSource={[]} />);

    it("passes `null` to the `notFoundContent` prop of the `Select`", () => {
      expect(wrap.find(Select).prop("notFoundContent")).toBe(null);
    });
  });

  describe("When input has been changed", () => {
    beforeEach(() => {
      props.form.setFieldTouched.mockClear();
    });

    const wrap = shallow(<SelectInput {...props} isLoading={false} dataSource={[]} />);

    it("sets the field to touched on blur", () => {
      wrap
        .find(Select)
        .props()
        .onBlur("test value");
      expect(props.form.setFieldTouched).toHaveBeenCalledWith("test", true);
    });
  });

  describe("When input has not been changed", () => {
    beforeEach(() => {
      props.form.setFieldTouched.mockClear();
    });

    const wrap = shallow(<SelectInput {...props} isLoading={false} dataSource={[]} />);

    it("does not set the field to touched on blur", () => {
      wrap
        .find(Select)
        .props()
        .onBlur("initial value");
      expect(props.form.setFieldTouched).not.toHaveBeenCalled();
    });
  });
});
