import React from "react";
import { shallow } from "enzyme";
import { Form } from "antd";
import FormItem from "./FormItem";

describe("<FormItem />", () => {
  const props = isTouched => ({
    name: "test",
    form: {
      errors: {
        test: "test error",
      },
      touched: {
        test: isTouched,
      },
    },
  });

  const textInput = (isTouched = false) =>
    shallow(
      <FormItem {...props(isTouched)}>
        <span>test</span>
      </FormItem>
    );

  describe("by default", () => {
    it("renders `children` prop inside a `Form.Item`", () => {
      expect(textInput().type()).toBe(Form.Item);
      expect(
        textInput()
          .children()
          .containsMatchingElement(<span>test</span>)
      ).toBeTruthy();
    });
  });

  describe("when field has not been touched but an error is present", () => {
    it("passes `false` to `FormItem` `hasFeedback` prop", () => {
      expect(textInput().prop("hasFeedback")).toBeFalsy();
    });

    it("passes `false` to `FormItem` `validateStatus` prop", () => {
      expect(textInput().prop("validateStatus")).toBeFalsy();
    });

    it("does not pass error message to `FormItem` `help` prop", () => {
      expect(textInput().prop("help")).toBeFalsy();
    });
  });

  describe("when field has been touched and an error occurred", () => {
    it("passes `true` to `FormItem` `hasFeedback` prop", () => {
      expect(textInput(true).prop("hasFeedback")).toBeTruthy();
    });

    it("passes `error` to `FormItem` `validateStatus` prop", () => {
      expect(textInput(true).prop("validateStatus")).toBe("error");
    });

    it("passes correct error message to `FormItem` `help` prop", () => {
      expect(textInput(true).prop("help")).toBe("test error");
    });
  });
});
