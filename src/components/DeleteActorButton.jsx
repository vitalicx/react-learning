import React from "react";
import PropTypes from "prop-types";
import { Button, Popconfirm } from "antd";

const DeleteActorButton = ({ loading, onDelete }) => (
  <Popconfirm
    placement="bottom"
    title="Are you sure to delete this actor?"
    onConfirm={onDelete}
    okText="Yes"
    cancelText="No"
  >
    <Button type="primary" size="small" className="mr-2" loading={loading} icon="minus">
      Delete Actor
    </Button>
  </Popconfirm>
);

DeleteActorButton.propTypes = {
  loading: PropTypes.bool.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default DeleteActorButton;
