import React from "react";
import { shallow } from "enzyme";
import MovieSearchForm from "./MovieSearchForm";
import SearchInputContainer from "../containers/inputs/SearchInputContainer";

describe("<MovieSearchForm />", () => {
  const handleSearch = jest.fn();
  const handleClear = jest.fn();
  const wrap = shallow(
    <MovieSearchForm isLoading={false} onSearch={handleSearch} onClear={handleClear} />
  );
  const innerForm = () =>
    wrap // Formik
      .dive() // ContextProvider
      .children(); // MovieSearchForm
  const fields = () => innerForm().dive();

  it("renders the `text` field", () => {
    expect(fields().prop("component")).toBe(SearchInputContainer);
  });

  it("passes correct default values", () => {
    expect(innerForm().prop("values")).toMatchObject({ text: "" });
  });

  it("prevents empty form submission", async () => {
    const errors = await innerForm()
      .props()
      .validateForm();
    expect(errors).toMatchObject({
      text: "Please enter a search term",
    });
  });

  describe("the rendered `text` field", () => {
    const field = fields().find({ name: "text" });

    it("receives `handleSearch` callback as `onSearch` prop", () => {
      field.props().onSearch();
      expect(handleSearch).toHaveBeenCalled();
    });

    it("receives `handleClear` callback as `onClear` prop", () => {
      field.props().onSearch();
      expect(handleSearch).toHaveBeenCalled();
    });

    it("receives parent `isLoading` prop", () => {
      expect(field.prop("isLoading")).toBe(false);
    });
  });
});
