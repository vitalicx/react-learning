import React from "react";
import { shallow } from "enzyme";
import HighlightedText from "./HighilghtedText";

describe("<HighlightedText />", () => {
  describe("when `filter` prop is not defined", () => {
    const wrap = shallow(<HighlightedText text="test" />);

    it("should return `text` prop", () => {
      expect(wrap.text()).toBe("test");
    });
  });

  describe("when `filter` prop is defined", () => {
    const wrap = shallow(<HighlightedText text="test" filter="test" />);

    it("should return highlighted text", () => {
      expect(wrap.containsMatchingElement(<span className="highlight">test</span>)).toBeTruthy();
    });
  });
});
