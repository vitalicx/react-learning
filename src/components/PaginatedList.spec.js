import React from "react";
import { shallow } from "enzyme";
import { withPaging, mapStateToProps } from "./PaginatedList";

describe("withPaging()", () => {
  const paginationState = {
    filter: { title: "test", sortField: "test", sortOrder: "desc" },
    loading: false,
    total: 5,
    currentPage: 1,
    pageSize: 5,
  };
  const actions = {
    requestPage: jest.fn(),
    resetPages: jest.fn(),
    setPageSize: jest.fn(),
    setPageSorting: jest.fn(),
  };

  const wrapped = () => <div />;
  const PaginatedList = withPaging(wrapped);

  describe("by default", () => {
    beforeEach(() => {
      for (let action of Object.values(actions)) {
        action.mockClear();
      }
    });

    const wrap = shallow(
      <PaginatedList pagination={paginationState} {...actions} additionalProp="test" />
    );

    it("renders the wrapped component", () => {
      expect(wrap.is(wrapped)).toBeTruthy();
    });

    it("passes `filter` property of the `pagination` prop to wrapped component", () => {
      expect(wrap.prop("filter")).toBe(paginationState.filter);
    });

    it("passes additional props through to wrapped component", () => {
      expect(wrap.prop("additionalProp")).toMatch("test");
    });

    it("removes action creators from props pass-through", () => {
      expect(wrap.props()).not.toHaveProperty("requestPage");
      expect(wrap.props()).not.toHaveProperty("resetPages");
      expect(wrap.props()).not.toHaveProperty("setPageSize");
      expect(wrap.props()).not.toHaveProperty("setPageSorting");
    });

    it("should pass the correct props from state", () => {
      const state = { paginations: { movies: paginationState } };
      const props = { stateKey: "movies" };
      expect(mapStateToProps(state, props)).toMatchObject({
        pagination: paginationState,
      });
    });

    describe("the injected `pagination` prop", () => {
      const pagination = wrap.prop("pagination");

      it("contains the required properties", () => {
        expect(pagination).toHaveProperty("showTotal");
        expect(pagination.total).toBe(5);
        expect(pagination.current).toBe(1);
        expect(pagination.pageSize).toBe(5);
      });

      it("contains a `showTotal` function that displays the correct paging text", () => {
        const text = pagination.showTotal(5, [1, 5]);
        expect(text).toMatch("1 to 5 of 5");
      });
    });

    describe("when a change event occurs", () => {
      const pagination = { current: 1, pageSize: 5 };
      const sorting = { field: "test", order: "desc" };

      describe("and the current page has changed", () => {
        beforeEach(() => {
          wrap.props().onChange({ ...pagination, current: 2 }, null, sorting);
        });
        it("calls `requestPage`", () => {
          expect(actions.requestPage).toHaveBeenCalledWith(2);
        });
        it("does not call `resetPages`", () => {
          expect(actions.resetPages).not.toHaveBeenCalled();
        });
      });

      describe("and the page size has changed", () => {
        beforeEach(() => {
          wrap.props().onChange({ ...pagination, pageSize: 10 }, null, sorting);
        });
        it("calls `setPageSize` and `resetPages`", () => {
          expect(actions.setPageSize).toHaveBeenCalledWith(10);
          expect(actions.resetPages).toHaveBeenCalled();
        });
      });

      describe("and the sort field has changed", () => {
        beforeEach(() => {
          wrap.props().onChange(pagination, null, { ...sorting, field: "test change" });
        });
        it("calls `setPageSorting` and `resetPages`", () => {
          expect(actions.setPageSorting).toHaveBeenCalledWith("test change", "desc");
          expect(actions.resetPages).toHaveBeenCalled();
        });
      });

      describe("and the sort order has changed", () => {
        beforeEach(() => {
          wrap.props().onChange(pagination, null, { ...sorting, order: "asc" });
        });
        it("calls `setPageSorting` and `resetPages`", () => {
          expect(actions.setPageSorting).toHaveBeenCalledWith("test", "asc");
          expect(actions.resetPages).toHaveBeenCalled();
        });
      });

      describe("and no changes occurred", () => {
        beforeEach(() => {
          wrap.props().onChange(pagination, null, sorting);
        });
        it("does not call any actions", () => {
          expect(actions.requestPage).not.toHaveBeenCalled();
          expect(actions.setPageSize).not.toHaveBeenCalled();
          expect(actions.setPageSorting).not.toHaveBeenCalled();
          expect(actions.resetPages).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe("when the page size is changed", () => {
    const pagination = { ...paginationState, currentPage: 2, total: 6 };
    const wrap = shallow(<PaginatedList pagination={pagination} {...actions} />);

    describe("and the current page is no longer valid", () => {
      it("should switch to the last valid page", () => {
        expect(wrap.instance().getCurrentPageAfterPageSizeChange(10)).toBe(1);
      });
    });

    describe("and the current page is still valid", () => {
      it("should not switch to the previous page", () => {
        expect(wrap.instance().getCurrentPageAfterPageSizeChange(5)).toBe(2);
      });
    });
  });
});
