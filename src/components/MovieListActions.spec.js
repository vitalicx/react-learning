/* eslint-disable react/jsx-key */
import React from "react";
import { shallow } from "enzyme";
import Animate from "rc-animate";
import MovieListActions from "./MovieListActions";
import DeleteButton from "./DeleteButton";
import IncreaseCountButton from "./IncreaseCountButton";

describe("<MovieListActions />", () => {
  let props, mountedComponent;

  const onIncreaseSpy = jest.fn();
  const onDeleteSpy = jest.fn();

  beforeEach(() => {
    onIncreaseSpy.mockClear();
    onDeleteSpy.mockClear();
    props = {
      movie: { id: 1 },
      actions: {},
      selectedRowKeys: [1],
      onIncrease: onIncreaseSpy,
      onDelete: onDeleteSpy,
    };
    mountedComponent = undefined;
  });

  const wrap = () =>
    mountedComponent || (mountedComponent = shallow(<MovieListActions {...props} />));

  it("should render a single `Animate` element", () => {
    expect(wrap().is(Animate)).toBeTruthy();
  });

  it("should display the number of items selected", () => {
    expect(wrap().containsMatchingElement(<span>1 item selected</span>)).toBeTruthy();
  });

  describe("when multiple items are selected", () => {
    beforeEach(() => {
      props.selectedRowKeys = [1, 2];
    });

    it("should display the number of items selected", () => {
      expect(wrap().containsMatchingElement(<span>2 items selected</span>)).toBeTruthy();
    });
  });

  it("should render a `DeleteButton`", () => {
    expect(wrap().find(DeleteButton).exists()).toBeTruthy();
  });

  it("should render an `IncreaseCountButton`", () => {
    expect(wrap().find(IncreaseCountButton).exists()).toBeTruthy();
  });

  describe("the rendered `DeleteButton`", () => {
    const deleteButton = () => wrap().find(DeleteButton);

    it("should call the passed `onDelete` prop on deletion", async () => {
      deleteButton().props().onDelete();
      expect(onDeleteSpy).toHaveBeenCalledWith(props.selectedRowKeys);
    });

    describe("by default", () => {
      it("should receive the correct loading and disabled state", () => {
        expect(deleteButton().prop("loading")).toBeFalsy();
        expect(deleteButton().prop("disabled")).toBeFalsy();
      });
    });

    describe("when actions are pending", () => {
      beforeEach(() => {
        props.actions.deleteMovie = [1];
      });

      it("should receive the correct loading and disabled state", () => {
        expect(deleteButton().prop("loading")).toBeTruthy();
        expect(deleteButton().prop("disabled")).toBeTruthy();
      });
    });
  });

  describe("the rendered `IncreaseCountButton`", () => {
    const increaseCountButton = () => wrap().find(IncreaseCountButton);

    it("should call the passed `onIncrease` prop when the count is increased", () => {
      increaseCountButton().props().onIncrease();
      expect(onIncreaseSpy).toHaveBeenCalledWith(props.selectedRowKeys);
    });

    describe("by default", () => {
      it("should receive the correct loading and disabled state", () => {
        expect(increaseCountButton().prop("loading")).toBeFalsy();
        expect(increaseCountButton().prop("disabled")).toBeFalsy();
      });
    });

    describe("when actions are pending", () => {
      beforeEach(() => {
        props.actions.deleteMovie = [1];
      });

      it("should receive the correct loading and disabled state", () => {
        expect(increaseCountButton().prop("loading")).toBeTruthy();
        expect(increaseCountButton().prop("disabled")).toBeTruthy();
      });
    });
  });

  describe("When no rows are selected", () => {
    beforeEach(() => {
      props.selectedRowKeys = [];
    });

    it("should pass nothing to `Animate`", () => {
      expect(wrap().children().length).toBe(0);
    });
  });
});
