import React from "react";
import PropTypes from "prop-types";
import { Button, Popconfirm } from "antd";

const DeleteButton = ({ loading, onDelete, ...props }) => (
  <Popconfirm
    placement="bottom"
    title="Are you sure you want to delete?"
    onConfirm={onDelete}
    okText="Yes"
    cancelText="No"
  >
    <Button icon="delete" type="primary" size="small" loading={loading} {...props}>
      Delete
    </Button>
  </Popconfirm>
);

DeleteButton.propTypes = {
  onDelete: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default DeleteButton;
