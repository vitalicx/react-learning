import React from "react";
import { Form, Button } from "antd";
import PropTypes from "prop-types";

export function withTemplate(fields) {
  return function TemplatedFormOuter(props) {
    return <TemplatedForm {...props}>{fields(props)}</TemplatedForm>;
  };
}

export const TemplatedForm = props => {
  const { children, dirty, handleSubmit, handleReset, isSubmitting } = props;
  return (
    <Form onSubmit={handleSubmit} noValidate>
      {children}
      <Form.Item>
        <Button
          type="button"
          className="outline"
          onClick={handleReset}
          disabled={!dirty || isSubmitting}
        >
          Reset
        </Button>
        <Button type="submit" htmlType="submit" disabled={isSubmitting}>
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

TemplatedForm.propTypes = {
  children: PropTypes.object.isRequired,
  dirty: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  handleReset: PropTypes.func,
  isSubmitting: PropTypes.bool,
};
