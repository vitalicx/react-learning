import React from "react";
import PropTypes from "prop-types";
import * as Yup from "yup";
import createForm from "../helpers/form";
import { Field, FastField } from "formik";
import TextInput from "./inputs/TextInput";
import SelectInput from "./inputs/SelectInput";

const formConfig = {
  displayName: "MovieForm",
  validationSchema: Yup.object().shape({
    title: Yup.string()
      .min(2, "C'mon, your name is longer than that")
      .required("Movie title is required."),
    actors: Yup.string()
      .required("At least one actor is required"),
  }),
  mapPropsToValues: props => ({ title: props.title || "", actors: [] }),
};

const MovieForm = createForm(formConfig)(props => (
  <div style={{ marginTop: "15px" }}>
    <FastField
      name="title"
      required
      autoComplete="off"
      placeholder="enter movie title"
      component={TextInput}
    />
    <Field
      name="actors"
      required
      autoComplete="off"
      placeholder="Select actors"
      component={SelectInput}
      onSearch={props.onSearch}
      dataSource={props.actors}
      isLoading={props.isLoading}
    />
  </div>
));

MovieForm.propTypes = {
  actors: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onSearch: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default MovieForm;