import React from "react";
import PropTypes from "prop-types";
import { Button } from "antd";

const IncreaseCountButton = ({ loading, onIncrease, ...props }) => (
  <Button
    type="primary"
    size="small"
    className="mr-2"
    loading={loading}
    onClick={onIncrease}
    icon="plus"
    {...props}
  >
    Increase Count
  </Button>
);

IncreaseCountButton.propTypes = {
  onIncrease: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default IncreaseCountButton;
