import React from "react";
import PropTypes from "prop-types";
import * as Yup from "yup";
import { Field } from "formik";
import SearchInputContainer from "../containers/inputs/SearchInputContainer";
import { withFormik } from "formik";

const formConfig = {
  displayName: "MovieSearchForm",
  validationSchema: Yup.object().shape({
    text: Yup.string().required("Please enter a search term"),
  }),
  mapPropsToValues: props => ({ text: props.text || "" }),
};

const MovieSearchForm = ({ onSearch, onClear, isLoading }) => (
  <Field
    name="text"
    required
    autoComplete="off"
    placeholder="Enter search text"
    component={SearchInputContainer}
    onSearch={onSearch}
    onClear={onClear}
    isLoading={isLoading}
  />
);

MovieSearchForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onSearch: PropTypes.func.isRequired,
  onClear: PropTypes.func.isRequired,
};

export default withFormik(formConfig)(MovieSearchForm);
