import React from "react";
import * as Yup from "yup";
import createForm from "../helpers/form";
import { FastField } from "formik";
import TextInput from "./inputs/TextInput";

const formConfig = {
  displayName: "LoginForm",
  validationSchema: Yup.object().shape({
    email: Yup.string().required("Email or username is required."),
    password: Yup.string()
      .min(2, "C'mon, your password is longer than that")
      .required("Password is required."),
  }),
  mapPropsToValues: props => ({ email: props.email || "", password: props.password || "" }),
};

export default createForm(formConfig)(() => (
  <>
    <FastField name="email" placeholder="enter username or email" component={TextInput} />
    <FastField
      name="password"
      type="password"
      autoComplete="off"
      placeholder="enter your password"
      component={TextInput}
    />
  </>
));
