/* eslint-disable react/jsx-key */
import React from "react";
import { shallow } from "enzyme";
import MovieListRowActions from "./MovieListRowActions";
import DeleteButton from "./DeleteButton";
import IncreaseCountButton from "./IncreaseCountButton";

describe("<MovieListRowActions />", () => {
  const movie = { id: 1 };
  const actions = {
    deleteMovie: [1],
    increaseCount: [1],
  };
  const onIncreaseSpy = jest.fn();
  const onDeleteSpy = jest.fn();
  const wrap = shallow(
    <MovieListRowActions
      movie={movie}
      actions={actions}
      onIncrease={onIncreaseSpy}
      onDelete={onDeleteSpy}
    />
  );

  it("should render a single `div` element", () => {
    expect(wrap.is("div")).toBeTruthy();
  });

  it("should render a `DeleteButton`", () => {
    expect(wrap.find(DeleteButton).exists()).toBeTruthy();
  });

  it("should render an `IncreaseCountButton`", () => {
    expect(wrap.find(IncreaseCountButton).exists()).toBeTruthy();
  });

  describe("the rendered `DeleteButton`", () => {
    let deleteButton;

    beforeEach(() => {
      deleteButton = wrap.find(DeleteButton);
    });

    it("should call the passed `onDelete` prop on deletion", async () => {
      deleteButton.props().onDelete();
      expect(onDeleteSpy).toHaveBeenCalledWith(movie);
    });

    it("should receive the correct loading state", () => {
      expect(deleteButton.prop("loading")).toBeTruthy();
    });
  });

  describe("the rendered `IncreaseCountButton`", () => {
    let increaseCountButton;

    beforeEach(() => {
      increaseCountButton = wrap.find(IncreaseCountButton);
    });

    it("should call the passed `onIncrease` prop when the count is increased", () => {
      increaseCountButton.props().onIncrease();
      expect(onIncreaseSpy).toHaveBeenCalledWith(movie);
    });

    it("should receive the correct loading state", () => {
      expect(increaseCountButton.prop("loading")).toBeTruthy();
    });
  });
});
