import React from "react";
import { shallow } from "enzyme";
import { Table } from "antd";
import MovieListActors from "./MovieListActors";
import DeleteActorButton from "./DeleteActorButton";

const Column = Table.Column;

describe("<MovieListActors />", () => {
  const actors = [
    {
      name: "sds",
      id: 94,
    },
    {
      name: "sss",
      id: 99,
    },
  ];

  const onDeleteSpy = jest.fn();

  const wrap = shallow(
    <MovieListActors actors={actors} onDelete={onDeleteSpy} isDeleting={jest.fn(() => true)} />
  );

  it("renders a `Table`", () => {
    expect(wrap.find(Table).exists()).toBeTruthy();
    expect(wrap.prop("dataSource")).toEqual(actors);
  });

  it("renders correct `Column`s", () => {
    const expectedColumns = {
      id: "ID",
      name: "Name",
      actions: "Actions",
    };
    const columns = wrap.children();
    for (const key in expectedColumns) {
      expect(
        columns.containsMatchingElement(<Column key={key} title={expectedColumns[key]} />)
      ).toBeTruthy();
    }
  });

  describe("the rendered Actions `Column`", () => {
    const deleteActorButton = wrap.wrap(wrap.find({ title: "Actions" }).props().render());

    it("renders a `DeleteActorButton`", () => {
      expect(deleteActorButton.type()).toBe(DeleteActorButton);
    });

    it("calls `onDelete` callback prop when an actor is deleted", () => {
      deleteActorButton.props().onDelete();
      expect(onDeleteSpy).toHaveBeenCalled();
    });

    it("calls `isDeleting` function prop to determine `DeleteActorButton` loading state", () => {
      expect(deleteActorButton.prop("loading")).toBeTruthy();
    });
  });
});
