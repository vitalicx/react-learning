import React from "react";
import PropTypes from "prop-types";
import { Table } from "antd";
import DeleteActorButton from "./DeleteActorButton";

const Column = Table.Column;
const MovieListActors = ({ actors, isDeleting, onDelete }) => (
  <Table dataSource={actors} rowKey="id" pagination={false}>
    <Column title="ID" dataIndex="id" key="id" />
    <Column title="Name" dataIndex="name" key="name" />
    <Column
      title="Actions"
      key="actions"
      render={(text, actor) => (
        <DeleteActorButton
          onDelete={() => onDelete(actor)}
          loading={isDeleting(actor)}
        />
      )}
    />
  </Table>
);

MovieListActors.propTypes = {
  actors: PropTypes.array.isRequired,
  isDeleting: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default MovieListActors;
