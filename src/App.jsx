import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { BrowserRouter as Router } from "react-router-dom";
import routes from "./routes";
import Navigation from "./components/navigation/Navigation";
import { logout } from "./actions/user";
import { getUser } from "./reducers/user";
import Notifications from "react-notification-system-redux";
import { Layout } from "antd";

const { Content } = Layout;

export const App = ({ user, logout, notifications }) => {
  return (
    <Router>
      <Layout>
        <Navigation user={user} onLogout={logout} />
        <Content>{routes}</Content>
        <Notifications notifications={notifications} />
      </Layout>
    </Router>
  );
};

App.propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  notifications: PropTypes.array.isRequired,
};

export const mapStateToProps = state => ({
  user: getUser(state),
  notifications: state.notifications,
});

export default connect(mapStateToProps, { logout })(App);
