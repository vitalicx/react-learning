import Types from "../constants/action-types";
import * as api from "../api/actors";
import { normalizeActors } from "./normalizer";
import convert from "./actorsConverter";

export const addActorSuccess = data => ({ type: Types.ADD_ACTOR, data });

export const addActor = (actor, token) => async dispatch => {
  try {
    const returnedActor = await api.addActor(convert(actor), token);
    dispatch(addActorSuccess(normalizeActors(returnedActor)));
  } catch (e) {
    throw e.violations;
  }
};
