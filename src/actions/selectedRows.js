import Types from "../constants/action-types";

export const setSelectedRows = (stateKey, keys) => ({
  type: Types.UI_SELECTED_ROWS_CHANGED,
  stateKey,
  keys,
});
