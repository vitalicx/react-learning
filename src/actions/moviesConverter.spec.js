import convert from "./moviesConverter";

describe("movies converter", () => {
  const movie = {
    title: "test",
    trackCount: 22,
    actors: [1, 2],
    id: 1,
  };

  it("should convert sub-resource entities to IRI references", () => {
    const converted = convert(movie);
    expect(converted.actors).toEqual(["/actors/1", "/actors/2"]);
  });
});
