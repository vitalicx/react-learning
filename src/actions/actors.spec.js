import * as api from "../api/actors";
import * as actions from "./actors";
import { mockErrorOnce } from "../helpers/testing";

jest.mock("../api/actors");
jest.mock("./normalizer", () => ({
  normalizeActors: actors => actors,
}));

describe("Actor actions", () => {
  const actors = [
    {
      name: "sds",
      id: 94,
      movies: [],
    },
    {
      name: "sss",
      id: 99,
      movies: [],
    },
  ];

  const dispatch = jest.fn();

  beforeEach(() => {
    dispatch.mockClear();
  });

  it("should create an action to add an actor", async () => {
    const thunk = actions.addActor(actors[0], "token");
    expect(thunk).toEqual(expect.any(Function));
    api.addActor.mockReturnValueOnce(actors[0]);
    await thunk(dispatch);
    expect(dispatch).toBeCalledWith(actions.addActorSuccess(actors[0]));

    const error = { violations: ["test"] };
    mockErrorOnce(api.addActor, error);
    try {
      await thunk(dispatch);
    } catch (e) {
      expect(e).toEqual(error.violations);
    }
    expect.assertions(3);
  });
});
