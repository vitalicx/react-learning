import * as api from "../api/user";
import * as actions from "./user";
import { decodeToken } from "../helpers/token";
import { message } from "antd";
import { mockErrorOnce } from "../helpers/testing";

jest.mock("../api/user");
jest.mock("antd", () => ({ message: { error: jest.fn() } }));

describe("User actions", () => {
  const dispatch = jest.fn();

  beforeEach(() => {
    dispatch.mockClear();
  });

  const tokenData = {
    token:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDE5NTIwNzEsImV4cCI6MTU0MTk1NTY3MSwicm9sZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfUkVBREVSIiwiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidml0YWxpYyJ9.eqXD8I5m_z-RkqaFjHWbgZMb0Fb4rnTUN4WCHrH7US1Xg29jmuqUeEdJ1b5dQKWawexssWIj0KLYZkKHvFCv2xa47CsCeWoN6FSiGIBS4GoYr-CXGX9mgWjn0cAxxl7ZYTcG3OnCM8Ks02RFVITGn2AIHcPSga2qtze095f8PINsxAFzqsg2g1p417AV_cRKb7vva5pQ-w1eBkJoO5nhqSRF_nwEqSJIeL77NJIUwFBmW1tBmxbyIxwwQv0rKO9lC_weQmU4oR1wD2WfG21DAe10izXIoK6hyu1-ABxgbuGCjoxmMIv0kgD1Q6Gq0rn2-op2RABxBNdz_8QGxeRsv7BMiy3JVK_ZO1RytrEUg95vIDnXLpUyrzuHfitoDhrK9InJB-ncszMdKsPXmGuMBo7PIUQLWwB2rgPj1dbD3_Yy8nnsOLdRMQ5dpI_7RJHXzXSZlaM7j9igW5dUjKeUoHKoLZh5hw_vd1hvoDaM0K1ZhlDwFR81txCrol4AnOm1HzvYeopaJn-6kn7q1WxyzjEiVBcWf5KtZLnon0I6IX52dJTipuwSA-VYmRfWb0ehU0k1wzQtUIidIR1eK9vOg6CqPFLTMAb3cMyvn0ZnLWO-QgsLvQ1mnJ5vdiZ2f6qZg2y1eaJC5WlQTEhA7j4WQqqvoxT3wkbDJmn7Ru9TgJU",
    refresh_token:
      "de0b64ef80f1a4baaf8ab567f444201d21425720101fdbf553283a17d5086f2965c2b741d3f94754edf28fd7010eaa4d79a98e67e6a5d68356ee61f1a09f6eac",
  };

  it("should create an action to login", async () => {
    const thunk = actions.login({ username: "test", password: "test" });
    expect(thunk).toEqual(expect.any(Function));
    api.login.mockReturnValueOnce(tokenData);
    const hide = jest.fn();
    message.loading = jest.fn(() => hide);
    await thunk(dispatch);
    expect(message.loading).toBeCalledWith("Logging in...", expect.any(Number));
    expect(dispatch).lastCalledWith(
      actions.loginSuccess(decodeToken(tokenData.token, tokenData.refresh_token))
    );
    expect(hide).toBeCalled();

    mockErrorOnce(api.login);
    await thunk(dispatch);
    expect(message.error).toBeCalledWith("Login failed: ");
  });

  it("should create an action to refresh a token", async () => {
    const thunk = actions.refresh(tokenData.refresh_token);
    expect(thunk).toEqual(expect.any(Function));
    api.refresh.mockReturnValueOnce(tokenData);
    await thunk(dispatch);
    const expectedPayload = { token: tokenData.token, refreshToken: tokenData.refresh_token };
    expect(dispatch).toBeCalledWith(actions.userRefreshedToken(expectedPayload));

    mockErrorOnce(api.refresh);
    await thunk(dispatch);
    expect(dispatch).toHaveBeenLastCalledWith(actions.logout());
  });
});
