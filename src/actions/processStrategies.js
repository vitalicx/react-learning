export const handleCircularReferences = key => (value, parent) => {
  return {
    ...value,
    ...(!!value[key] && {
      [key]: value[key].map(item => (typeof item === "string" ? parent.id : item)),
    }),
  };
};
