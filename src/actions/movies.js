import Types from "../constants/action-types";
import * as api from "../api/movies";
import { getErrorMessage } from "../api/error";
import { error, success } from "react-notification-system-redux";
import { normalizeMovies } from "./normalizer";
import convert from "./moviesConverter";

export const addMovieSuccess = data => ({ type: Types.ADD_MOVIE, data });

export const increaseCountRequest = id => ({ type: Types.INCREASE_COUNT_REQUEST, id });
export const increaseCountSuccess = data => ({ type: Types.INCREASE_COUNT_SUCCESS, data });
export const increaseCountFailure = (error, id) => ({ type: Types.INCREASE_COUNT_FAILURE, error, id });

export const deleteMovieRequest = id => ({ type: Types.DELETE_MOVIE_REQUEST, id });
export const deleteMovieSuccess = id => ({ type: Types.DELETE_MOVIE_SUCCESS, id });
export const deleteMovieFailure = (error, id) => ({ type: Types.DELETE_MOVIE_FAILURE, error, id });
export const deleteMoviesCompleted = () => ({ type: Types.DELETE_MOVIES_COMPLETED });

export const deleteActorRequest = (movieId, actorId) => ({ type: Types.DELETE_ACTOR_REQUEST, movieId, actorId });
export const deleteActorSuccess = (data, actorId) => ({ type: Types.DELETE_ACTOR_SUCCESS, data, actorId });
export const deleteActorFailure = (error, movieId, actorId) => ({ type: Types.DELETE_ACTOR_FAILURE, error, movieId, actorId});

export const increaseCountMultiple = (ids, token) => ({
  type: Types.INCREASE_COUNT_MULTIPLE,
  ids,
  token,
});

export const deleteMovieMultiple = (ids, token) => ({
  type: Types.DELETE_MOVIE_MULTIPLE,
  ids,
  token,
});

export const addMovie = (movie, token) => async dispatch => {
  try {
    const returnedMovie = await api.addMovie(convert(movie), token);
    dispatch(addMovieSuccess(normalizeMovies(returnedMovie)));
    dispatch(success({ title: "Added movie successfully" }));
  } catch (e) {
    throw e.violations;
  }
};

export const doIncreaseCount = (movie, token) =>
  api.updateMovie(movie.id, { trackCount: movie.trackCount + 1 }, token);

export const increaseCount = (movie, token) => async dispatch => {
  try {
    dispatch(increaseCountRequest(movie.id));
    const returnedMovie = await doIncreaseCount(movie, token);
    dispatch(increaseCountSuccess(normalizeMovies(returnedMovie)));
    dispatch(success({ title: "Updated movie successfully" }));
  } catch (e) {
    dispatch(increaseCountFailure(e, movie.id));
    dispatch(error({ title: getErrorMessage(e) }));
  }
};

export const deleteActor = (movie, actor, token) => async dispatch => {
  const actorId = actor.id;
  const movieId = movie.id;
  try {
    dispatch(deleteActorRequest(movieId, actorId));
    const returnedMovie = await api.updateMovie(
      movieId,
      convert({ actors: movie.actors.filter(id => id !== actorId) }),
      token
    );
    dispatch(deleteActorSuccess(normalizeMovies(returnedMovie), actorId));
    dispatch(success({ title: "Updated movie successfully" }));
  } catch (e) {
    dispatch(deleteActorFailure(e, movieId, actorId));
    dispatch(error({ title: getErrorMessage(e) }));
  }
};

export const deleteMovie = (id, token) => async dispatch => {
  try {
    dispatch(deleteMovieRequest(id));
    await api.deleteMovie(id, token);
    dispatch(deleteMovieSuccess(id));
    dispatch(deleteMoviesCompleted());
    dispatch(success({ title: "Deleted movie successfully" }));
  } catch (e) {
    dispatch(deleteMovieFailure(e, id));
    dispatch(error({ title: getErrorMessage(e) }));
  }
};