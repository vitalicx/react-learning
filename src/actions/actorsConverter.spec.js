import convert from "./actorsConverter";

describe("actors converter", () => {
  const actor = {
    name: "test",
    movies: [1, 2],
    id: 1,
  };

  it("should convert sub-resource entities to IRI references", () => {
    const converted = convert(actor);
    expect(converted.movies).toEqual(["/movies/1", "/movies/2"]);
  });
});
