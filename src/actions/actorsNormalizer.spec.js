import { normalizeActors } from "./normalizer";

describe("Actors normalizer", () => {
  const actors = [
    {
      name: "sds",
      id: 94,
    },
    {
      name: "sss",
      id: 99,
      movies: [
        {
          id: 55,
          actors: [
            { id: 93, name: "nested actor" },
            // Reference to parent actor
            "/actors/99",
          ],
        },
      ],
    },
  ];

  it("should normalize a single actor", () => {
    const normalized = normalizeActors(actors[0]);
    expect(normalized).toHaveProperty(["entities", "actors", "94"]);
  });

  it("should normalize an array of actors", () => {
    const normalized = normalizeActors(actors);
    expect(normalized).toHaveProperty(["entities", "actors", "94"]);
    expect(normalized).toHaveProperty(["entities", "actors", "99"]);
  });

  it("should normalize nested actors", () => {
    const normalized = normalizeActors(actors);
    expect(normalized).toHaveProperty(["entities", "actors", "93"]);
    expect(normalized).toHaveProperty(["entities", "movies", "55"]);
    expect(normalized.entities.movies[55].actors).toMatchObject([93, 99]);
  });
});
