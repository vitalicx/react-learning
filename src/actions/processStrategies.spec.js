import { handleCircularReferences } from "./processStrategies";

describe("Process strategies", () => {
  describe("When an entity contains a circular reference", () => {
    const movie = { id: 100 };

    const actorOne = {
      id: 1,
      movies: [
        // Reference to parent movie
        "/movies/100",
      ],
    };

    const actorTwo = {
      id: 1,
      movies: [
        // Regular entity object
        {
          id: 101,
          title: "nested movie",
        },
      ],
    };

    it("should replace it with the parent entity", () => {
      const handler = handleCircularReferences("movies");
      const actor = handler(actorOne, movie);
      expect(actor.movies).toMatchObject([100]);
    });

    it("should leave regular entity object untouched", () => {
      const handler = handleCircularReferences("movies");
      const actor = handler(actorTwo, movie);
      expect(actor.movies[0].title).toMatch("nested movie");
    });
  });
});
