import normalizeActors, { actorSchema, actors } from "./actorsNormalizer";
import normalizeMovies, { movieSchema, movies } from "./moviesNormalizer";

movieSchema.define({ actors });
actorSchema.define({ movies });

export { normalizeActors, normalizeMovies };
