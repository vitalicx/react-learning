import Types from "../../constants/action-types";
import * as api from "../../api/actors";
import { normalizeActors } from "../normalizer";

export const requestingActors = filter => ({ type: Types.UI_SELECT_ACTORS_SEARCH_REQUEST, filter });
export const receivedActors = data => ({ type: Types.UI_SELECT_ACTORS_SEARCH_SUCCESS, data });
export const requestActorsFailure = error => ({ type: Types.UI_SELECT_ACTORS_SEARCH_FAILURE, error });

export const requestActors = (token, filter) => async dispatch => {
  try {
    dispatch(requestingActors(filter));
    const movies = await api.requestActors(token, filter);
    dispatch(receivedActors(normalizeActors(movies)));
  } catch (e) {
    dispatch(requestActorsFailure(e));
  }
};