import * as api from "../../api/actors";
import * as actions from "./actors";
import { mockErrorOnce } from "../../helpers/testing";

jest.mock("../../api/actors");
jest.mock("../normalizer", () => ({
  normalizeActors: actors => actors,
}));

describe("Actor actions", () => {
  const actors = [
    {
      name: "sds",
      id: 94,
    },
    {
      name: "sss",
      id: 99,
    },
  ];

  const dispatch = jest.fn();

  beforeEach(() => {
    dispatch.mockClear();
  });

  it("should create an action to request and receive actors", async () => {
    const thunk = actions.requestActors("token", "search text");
    expect(thunk).toEqual(expect.any(Function));
    api.requestActors.mockReturnValueOnce(actors);
    await thunk(dispatch);
    expect(dispatch).toBeCalledWith(actions.requestingActors("search text"));
    expect(dispatch).lastCalledWith(actions.receivedActors(actors));

    mockErrorOnce(api.requestActors);
    await thunk(dispatch);
    expect(dispatch).lastCalledWith(actions.requestActorsFailure(Error("")));
  });
});
