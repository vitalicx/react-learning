import * as api from "../../api/movies";
import * as actions from "./movies";
import { mockErrorOnce } from "../../helpers/testing";

jest.mock("../../api/movies");
jest.mock("../normalizer", () => ({
  normalizeMovies: movies => movies,
}));

describe("Movie actions", () => {
  const movies = [
    {
      title: "sds",
      id: 94,
    },
    {
      title: "sss",
      id: 99,
    },
  ];

  const dispatch = jest.fn();

  beforeEach(() => {
    dispatch.mockClear();
  });

  it("should create an action to request and receive movies", async () => {
    const thunk = actions.requestMovies("token", "search text");
    expect(thunk).toEqual(expect.any(Function));
    api.requestMovies.mockReturnValueOnce(movies);
    await thunk(dispatch);
    expect(dispatch).toBeCalledWith(actions.requestingMovies("search text"));
    expect(dispatch).lastCalledWith(actions.receivedMovies(movies));

    mockErrorOnce(api.requestMovies);
    await thunk(dispatch);
    expect(dispatch).lastCalledWith(actions.requestMoviesFailure(Error("")));
  });
});
