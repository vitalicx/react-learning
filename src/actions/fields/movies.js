import Types from "../../constants/action-types";
import * as api from "../../api/movies";
import { normalizeMovies } from "../normalizer";

export const requestingMovies = filter => ({ type: Types.UI_SELECT_MOVIES_SEARCH_REQUEST, filter });
export const receivedMovies = data => ({ type: Types.UI_SELECT_MOVIES_SEARCH_SUCCESS, data });
export const requestMoviesFailure = error => ({ type: Types.UI_SELECT_MOVIES_SEARCH_FAILURE, error });

export const requestMovies = (token, filter) => async dispatch => {
  try {
    dispatch(requestingMovies(filter));
    const movies = await api.requestMovies(token, filter);
    dispatch(receivedMovies(normalizeMovies(movies)));
  } catch (e) {
    dispatch(requestMoviesFailure(e));
  }
};