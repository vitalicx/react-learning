import * as api from "../api/movies";
import * as actions from "./movies";
import { mockErrorOnce } from "../helpers/testing";
import convert from "./moviesConverter";

jest.mock("../api/movies");
jest.mock("./normalizer", () => ({
  normalizeMovies: movies => movies,
}));

describe("Movie actions", () => {
  const movies = {
    0: {
      title: "sds",
      trackCount: 22,
      id: 0,
      actors: [0],
    },
    1: {
      title: "sss",
      trackCount: 16,
      id: 1,
      actors: [0],
    },
    2: {
      title: "sssdad",
      trackCount: 23,
      id: 2,
      actors: [0],
    },
  };

  const dispatch = jest.fn();

  beforeEach(() => {
    dispatch.mockClear();
    api.updateMovie.mockClear();
    api.deleteMovie.mockClear();
  });

  describe("when adding a movie", () => {
    describe("the action creator", () => {
      const error = { violations: ["test"] };
      const thunk = actions.addMovie(movies[0], "token");

      it("should return a thunk", () => {
        expect(thunk).toEqual(expect.any(Function));
      });

      describe("when dispatched and successful", () => {
        beforeEach(async () => {
          api.addMovie.mockReturnValueOnce(movies[0]);
          await thunk(dispatch);
        });

        it("should call the API", () => {
          expect(api.addMovie).toHaveBeenCalledWith(convert(movies[0]), "token");
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(actions.addMovieSuccess(movies[0]));
        });

        it("should dispatch a success message", () => {
          expect(dispatch).lastCalledWith(
            expect.objectContaining({ title: expect.stringContaining("success") })
          );
        });
      });

      describe("when dispatched and an error occurs", () => {
        beforeEach(() => {
          mockErrorOnce(api.addMovie, error);
        });

        it("should catch the error and throw its `violations` property", async () => {
          try {
            await thunk(dispatch);
          } catch (e) {
            expect(e).toEqual(error.violations);
          }
          expect.assertions(1);
        });
      });
    });
  });

  describe("when increasing a movie's track count", () => {
    describe("the action creator", () => {
      const error = { message: "test" };
      const thunk = actions.increaseCount(movies[0], "token");
      const returnedMovie = { ...movies[0], trackCount: movies[0].trackCount + 1 };

      it("should return a thunk", () => {
        expect(thunk).toEqual(expect.any(Function));
      });

      describe("when dispatched and successful", () => {
        beforeEach(async () => {
          api.updateMovie.mockReturnValueOnce(returnedMovie);
          await thunk(dispatch);
        });

        it("should call the API", () => {
          expect(api.updateMovie).toHaveBeenCalledWith(
            movies[0].id,
            { trackCount: movies[0].trackCount + 1 },
            "token"
          );
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(actions.increaseCountRequest(movies[0].id));
          expect(dispatch).toHaveBeenCalledWith(actions.increaseCountSuccess(returnedMovie));
        });

        it("should dispatch a success message", () => {
          expect(dispatch).lastCalledWith(
            expect.objectContaining({ title: expect.stringContaining("success") })
          );
        });
      });

      describe("when dispatched and an error occurs", () => {
        beforeEach(async () => {
          mockErrorOnce(api.updateMovie, error);
          await thunk(dispatch);
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(actions.increaseCountFailure(error, movies[0].id));
        });

        it("should dispatch an error message", () => {
          expect(dispatch).lastCalledWith(expect.objectContaining({ title: error.message }));
        });
      });
    });
  });

  describe("when deleting a movie", () => {
    describe("the action creator", () => {
      const movieId = 0;
      const error = { message: "test" };
      const thunk = actions.deleteMovie(movieId, "token");

      it("should return a thunk", () => {
        expect(thunk).toEqual(expect.any(Function));
      });

      describe("when dispatched and successful", () => {
        beforeEach(async () => {
          await thunk(dispatch);
        });

        it("should call the API", () => {
          expect(api.deleteMovie).toHaveBeenCalledWith(movieId, "token");
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(actions.deleteMovieRequest(movieId));
          expect(dispatch).toHaveBeenCalledWith(actions.deleteMovieSuccess(movieId));
        });

        it("should dispatch a success message", () => {
          expect(dispatch).lastCalledWith(
            expect.objectContaining({ title: expect.stringContaining("success") })
          );
        });
      });

      describe("when dispatched and an error occurs", () => {
        beforeEach(async () => {
          mockErrorOnce(api.deleteMovie, error);
          await thunk(dispatch);
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(actions.deleteMovieFailure(error, movieId));
        });

        it("should dispatch an error message", () => {
          expect(dispatch).lastCalledWith(expect.objectContaining({ title: error.message }));
        });
      });
    });
  });


  describe("when deleting an actor from a movie", () => {
    describe("the action creator", () => {
      const error = { message: "test" };
      const thunk = actions.deleteActor(movies[0], { id: movies[0].actors[0] }, "token");
      const returnedMovie = { ...movies[0], actors: [] };

      it("should return a thunk", () => {
        expect(thunk).toEqual(expect.any(Function));
      });

      describe("when dispatched and successful", () => {
        beforeEach(async () => {
          api.updateMovie.mockReturnValueOnce(returnedMovie);
          await thunk(dispatch);
        });

        it("should call the API", () => {
          expect(api.updateMovie).toHaveBeenCalledWith(movies[0].id, { actors: [] }, "token");
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(
            actions.deleteActorRequest(movies[0].id, movies[0].actors[0])
          );
          expect(dispatch).toHaveBeenCalledWith(
            actions.deleteActorSuccess(returnedMovie, movies[0].actors[0])
          );
        });

        it("should dispatch a success message", () => {
          expect(dispatch).lastCalledWith(
            expect.objectContaining({ title: expect.stringContaining("success") })
          );
        });
      });

      describe("when dispatched and an error occurs", () => {
        beforeEach(async () => {
          mockErrorOnce(api.updateMovie, error);
          await thunk(dispatch);
        });

        it("should dispatch the correct actions", () => {
          expect(dispatch).toHaveBeenCalledWith(
            actions.deleteActorFailure(error, movies[0].id, movies[0].actors[0])
          );
        });

        it("should dispatch an error message", () => {
          expect(dispatch).lastCalledWith(expect.objectContaining({ title: error.message }));
        });
      });
    });
  });
});
