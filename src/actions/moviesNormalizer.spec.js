import { augment } from "./moviesNormalizer";
import { normalizeMovies } from "./normalizer";

describe("Movies normalizer", () => {
  const movies = [
    {
      title: "sds",
      trackCount: 22,
      id: 94,
      actors: [1],
    },
    {
      title: "sss",
      trackCount: 16,
      id: 99,
      actors: [1],
    },
    {
      title: "sssdad",
      trackCount: 23,
      id: 101,
      actors: [
        {
          id: 10,
          movies: [
            {
              id: 103,
              title: "nested movie",
            },
            // Reference to parent movie
            "/movies/101",
          ],
        },
      ],
    },
  ];

  it("should normalize a single movie", () => {
    const normalized = normalizeMovies(movies[0]);
    expect(normalized).toHaveProperty(["entities", "movies", "94"]);
  });

  it("should normalize an array of movies", () => {
    const normalized = normalizeMovies(movies);
    expect(normalized).toHaveProperty(["entities", "movies", "94"]);
    expect(normalized).toHaveProperty(["entities", "movies", "101"]);
  });

  it("should normalize nested movies", () => {
    const normalized = normalizeMovies(movies);
    expect(normalized).toHaveProperty(["entities", "movies", "103"]);
    expect(normalized).toHaveProperty(["entities", "actors", "10"]);
    expect(normalized.entities.actors[10].movies).toMatchObject([103, 101]);
  });

  it("should augment a movie with a numActors field", () => {
    const result = augment(movies[0]);
    expect(result).toHaveProperty("numActors");
    expect(result.numActors).toBe(1);
  });

  it("should augment an array of movies with a numActors field", () => {
    const result = augment(movies);
    expect(result[0]).toHaveProperty("numActors");
    expect(result[0].numActors).toBe(1);
  });
});
