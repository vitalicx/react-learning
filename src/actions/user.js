import Types from "./../constants/action-types";
import * as api from "../api/user";
import { decodeToken } from "../helpers/token";
import { message } from "antd";

export const loginSuccess = data => ({ type: Types.USER_LOGGED_IN, data });
export const loggingIn = () => ({ type: Types.USER_LOGGING_IN });
export const logout = () => ({ type: Types.USER_LOGGED_OUT });
export const userRefreshedToken = tokens => ({ type: Types.USER_REFRESHED_TOKEN, tokens });

export const login = data => async dispatch => {
  const hide = message.loading("Logging in...", 0);
  try {
    const { token, refresh_token } = await api.login(data);
    dispatch(loginSuccess(decodeToken(token, refresh_token)));
    message.success("Logged in successfully");
  } catch (e) {
    message.error(`Login failed: ${e.message}`);
  } finally {
    hide();
  }
};

export const refresh = refreshToken => async dispatch => {
  try {
    const { token } = await api.refresh(refreshToken);
    dispatch(userRefreshedToken({ token, refreshToken }));
  } catch (error) {
    dispatch(logout());
  }
};
