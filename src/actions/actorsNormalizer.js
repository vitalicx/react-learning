import { schema, normalize } from "normalizr";
import { handleCircularReferences } from "./processStrategies";

export const actorSchema = new schema.Entity(
  "actors",
  {},
  { processStrategy: handleCircularReferences("movies") }
);

export const actors = new schema.Array(actorSchema);

export const getEntities = data => data.entities.actors || {};
//export const getEntityId = data => data.result;

export default result => normalize(result, Array.isArray(result) ? actors : actorSchema);
