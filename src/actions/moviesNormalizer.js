import { schema, normalize } from "normalizr";
import { handleCircularReferences } from "./processStrategies";

export const movieSchema = new schema.Entity(
  "movies",
  {},
  { processStrategy: handleCircularReferences("actors") }
);

export const movies = new schema.Array(movieSchema);

export const getEntities = data => data.entities.movies || {};
export const getEntityId = data => data.result;

const augmentMovie = movie => ({ ...movie, numActors: movie.actors.length });
export const augment = data => (Array.isArray(data) ? data.map(augmentMovie) : augmentMovie(data));

export default result => normalize(augment(result), Array.isArray(result) ? movies : movieSchema);
